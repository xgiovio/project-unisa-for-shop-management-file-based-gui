package it.unisa.info13d;

import it.unisa.info13d.GestioneCatalogo.Catalogo;
import it.unisa.info13d.Gui.LoginWindow;
import it.unisa.info13d.Login.Access;
import it.unisa.info13d.Login.LoggedUser;
import it.unisa.info13d.Login.LoginData;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import it.unisa.info13d.Utility.IdCounter;
import it.unisa.info13d.Utility.ReShow;

/**
 * Questa classe rappresenta il Main dell'applicazione dalla quale si avviera' l'interfaccia di interazione con l'utente.
 * 
 * @author Simone Argenziano e Giovanni Di Grezia
 * 
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException,IOException,ClassNotFoundException, ParseException {


        LoginWindow l = new LoginWindow();

    }
}