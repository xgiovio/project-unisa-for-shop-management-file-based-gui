package it.unisa.info13d.Login;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 16/12/13
 * Time: 19.45
 */
public class Access {
    /**
     * Questo metodo avvia la procedura di login o di registrazione dell'utente in base alla scelta fatta nel menu.
     *
     * @param set_user_logged_here dove memorizzare il nome dell'utente loggato o registrato
     * @return false se Amministratore, true se Utente
     */
    public static boolean get_access (LoggedUser set_user_logged_here, String in_name,String in_password,String in_location,boolean in_action) throws  FileNotFoundException,IOException,ClassNotFoundException{

        if  (in_action == false){
            return login(set_user_logged_here,in_name,in_password);
        }
        return register(set_user_logged_here,in_name,in_password,in_location);
    }

    /**
     * Metodo che si occupa di definire l'utente loggato (Amministratore o utente)
     * 
     * @param set_user_logged_here nome dell'utente loggato o registrato
     * @return Se avviene una registrazione viene restituito il valore true, poiche solo gli utenti possono registrarsi. Se viene effettuato un login, viene restituito il tipo di utente appena loggato.
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    protected static boolean login(LoggedUser set_user_logged_here, String in_name,String in_password) throws  FileNotFoundException,IOException,ClassNotFoundException{

        Scanner reader = new Scanner(System.in);
        String name;
        String password;


        name = in_name;
        password = in_password;
      set_user_logged_here.logged_user = name;
      return getAccountType(name);

    }

    /**
     * Metodo che si occupa di finalizzare la registrazione dell'utente salvandolo sul disco.
     * 
     * @param set_user_logged_here nome dell'utente loggato o registrato
     * @return true poiche' e' possibile la registrazione solo degli utenti. 
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    protected static boolean register ( LoggedUser set_user_logged_here, String in_name,String in_password,String in_location ) throws  FileNotFoundException,IOException,ClassNotFoundException{
        String name = in_name;
        String password = in_password;
        String location = in_location;

        store_data(name,password,location);

        set_user_logged_here.logged_user = name;
        return true;
    }

    /**
     * Questo metodo si occupa della ricerca dei dati dell'utente all'interno del "database".
     *
     * @param in_name username dell'utente
     * @param in_password password dell'utente
     * @return  true se la ricerca ha avuto esito, false altrimenti
     */
    public static boolean search_username (String in_name, String in_password) throws  FileNotFoundException,IOException,ClassNotFoundException{

        File f_data = new File("user_db");
        if (  (f_data.exists())){
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            ArrayList<Entry> database = (ArrayList<Entry>) reader.readObject();
            for (int i = 0 ; i < database.size();++i){
                if  (
                        database.get(i).getUser().equals( in_name) &&
                        database.get(i).getPassword().equals(in_password)
                    )
                {
                 reader.close();
                 return true;
                }
            }
            reader.close();
            return false;

        }else{
            ArrayList<Entry> database = new ArrayList<Entry>();
            database.add(new Entry("admin","admin",false,"UNISA"));

            ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(f_data));
            writer.writeObject(database);
            writer.close();

            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            database = (ArrayList<Entry>) reader.readObject();
            for (int i = 0 ; i < database.size();++i){
                if  (
                        database.get(i).getUser().equals( in_name) &&
                                database.get(i).getPassword().equals(in_password)
                        )
                {
                    reader.close();
                    return true;
                }
            }
            reader.close();
            return false;
        }
    }

    /**
     * Metodo che si occupa di restituite la tipologia dell'account dell'utente connesso
     *
     * @param in_name username utente
     * @return  Tipo di account (admin o Client)
     */
    protected static boolean getAccountType(String in_name) throws  FileNotFoundException,IOException,ClassNotFoundException{

        File f_data = new File("user_db");
        if (  (f_data.exists())){
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            ArrayList<Entry> database = (ArrayList<Entry>) reader.readObject();
            for (int i = 0 ; i < database.size();++i){
                if  (  database.get(i).getUser().equals( in_name)   )
                {
                    reader.close();
                    return database.get(i).getType();

                }
            }
            reader.close();
        }
        return false;
    }

    /**
     * Questo metodo si occupa di salvare i dati su disco dell'utente.
     *
     * @param in_nome username utente
     * @param in_password password utente
     */
    protected static void store_data (String in_nome, String in_password, String in_location) throws  FileNotFoundException,IOException,ClassNotFoundException{

        File f_data = new File("user_db");
        if (  (f_data.exists())){
            //Aggiungi dati al database
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            ArrayList<Entry> database = (ArrayList<Entry>) reader.readObject();
            database.add(new Entry(in_nome,in_password,true,in_location));
            reader.close();
            ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(f_data));
            writer.writeObject(database);
            writer.close();
        }else{
           //Crea nuovo database e poi memorizza
            ArrayList<Entry> database = new ArrayList<Entry>();
            database.add(new Entry("admin","admin",false,"UNISA"));
            database.add(new Entry(in_nome,in_password,true,in_location));

            ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(f_data));
            writer.writeObject(database);
            writer.close();
        }
    }

    /**
     * Questo metodo si occupa di prelevare i dati dell'utente dal disco.
     * 
     * @param in_nome username utente
     * @return un Entry contenente tutti i dati dell'utente
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static  Entry get_user_data (String in_nome)throws  FileNotFoundException,IOException,ClassNotFoundException{

        File f_data = new File("user_db");
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            ArrayList<Entry> database = (ArrayList<Entry>) reader.readObject();
            for (int i = 0 ; i < database.size();++i){
                if  ( database.get(i).getUser().equals( in_nome)  )
                {
                    reader.close();
                    return database.get(i);
                }
            }
        return null; // Non puoi essere qui o altrimenti il tuo database e' corrotto
    }

    /**
     * Questo metodo scrive su disco le modifiche apportate agli attributi di un utente. (Ad esempio bilancio incrementato)
     * 
     * @param in_nome Username utente
     * @param new_entry Oggetto di tipo Entry
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void replace_data (String in_nome, Entry new_entry )throws  FileNotFoundException,IOException,ClassNotFoundException{
        File f_data = new File("user_db");
        ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
        ArrayList<Entry> database = (ArrayList<Entry>) reader.readObject();
        for (int i = 0 ; i < database.size();++i){
            if  ( database.get(i).getUser().equals( in_nome)  )
            {
                reader.close();
                database.set(i,new_entry);
                break;

            }
        }
        ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(f_data));
        writer.writeObject(database);
        writer.close();
    }
}
