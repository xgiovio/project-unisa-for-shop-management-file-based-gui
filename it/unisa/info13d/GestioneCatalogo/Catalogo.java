package it.unisa.info13d.GestioneCatalogo;
import java.util.*;

import it.unisa.info13d.Articoli.*;
import it.unisa.info13d.Gui.UserInterface;
import it.unisa.info13d.Login.Access;
import it.unisa.info13d.Login.Entry;
import it.unisa.info13d.Starter;
import it.unisa.info13d.Utility.IdCounter;
import it.unisa.info13d.Utility.ReShow;
import it.unisa.info13d.Login.Access;
import it.unisa.info13d.Login.LoggedUser;
import it.unisa.info13d.Login.LoginData;
import it.unisa.info13d.Login.StoricoItem;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created with PC-STATION.
 * User: lebon
 * Date: 17/12/13
 * Time: 10.45
 */
public class Catalogo{

	/**
	 * Il costruttore definisce un nuovo catalogo nel caso non sia gia presente.
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public Catalogo() throws FileNotFoundException,IOException,ClassNotFoundException
	{
        File f_data = new File("cat_db");
        if (  (f_data.exists())){
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            catalogoOfferte = (ArrayList<Utilizzabile>) reader.readObject();
            reader.close();
        } else {
            catalogoOfferte = new ArrayList<Utilizzabile>();
            //comment start to not auto populate the catalog
            Starter populate_item = new Starter(this);
        }
    }

	/**
	 * Questo metodo si occupa di serializzare il catalogo (per garantire un minimo di sicurezza) e salvarlo su disco.
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
    void store_data()throws FileNotFoundException,IOException,ClassNotFoundException{
        File f_data = new File("cat_db");
        ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(f_data));
        writer.writeObject(catalogoOfferte);
        writer.close();
    }

	
	/**
	 * Questo metodo seleziona in Menu da visualizzare in base al login effettuato dall'utente.
	 * 
	 * @param userType e' il tipo di utente ottenuto dal mail.
	 * @throws IOException 
	 * @throws ParseException 
	 * @throws ClassNotFoundException 
	 * @throws FileNotFoundException 
	 */
	public void showMenu(String userType,String username, ReShow r) throws FileNotFoundException, ClassNotFoundException, ParseException, IOException
	{
		if(userType.equals("Admin"))
			AdminSession.showAdminMenu(this,r, username);
		else
			ClientSession.showClientMenu(this,r, username);
	}
	
	/**
	 * Questo metodo permette l'inserimento di un nuovo deal all'interno del catalogo.
	 * 
	 * @throws ParseException 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws FileNotFoundException 
	 */
	public void nuovoProdottoVacanza(String in_localita, String in_data_start, String in_data_end, String in_prezzo) throws ParseException, FileNotFoundException, ClassNotFoundException, IOException
	{
				//System.out.println("---------- Nuova Offerta Vacanza ----------");
				//System.out.print("Localita: ");
				String localita = in_localita;
				
				//System.out.print("Data Partenza (GG/MM/AAAA): ");
				 String data = in_data_start;
				 String temp[];
				 String marcatore="/";
				 temp = data.split(marcatore);
				 GregorianCalendar dataPartenza = new GregorianCalendar((Integer.parseInt(temp[2])),(Integer.parseInt(temp[1]))-1,(Integer.parseInt(temp[0])));
							
				//System.out.print("Data Scadenza offerta (GG/MM/AAAA): ");
				 String data2 = in_data_end;
				 String temp2[];
				 temp2 = data2.split(marcatore);
				 GregorianCalendar dataScad = new GregorianCalendar((Integer.parseInt(temp2[2])),(Integer.parseInt(temp2[1]))-1,(Integer.parseInt(temp2[0])));
								
				//System.out.print("Prezzo singola persona: ");
				String prezzo = in_prezzo;
				double prezzoPSing = Double.parseDouble(prezzo);
								
				catalogoOfferte.add(new Vacanze(Global.get_next_id(), localita, dataPartenza, dataScad, prezzoPSing));
				store_data();

        IdCounter.save_counter();
	}
	
	/**
	 * Questo metodo aggiunge al cataologo una nuova cena.
	 * 
	 * @param in_ristorante Nome ristorante
	 * @param in_localita localit� del ristorante
	 * @param in_description descrizione della cena
	 * @param in_prezzo prezzo cena
	 * @param in_data_scad data di scadenza della cena
	 * @param in_cene_da_vend numero di cene da vendere
	 * @throws ParseException
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
    public void nuovoProdottoCena(String in_ristorante, String in_localita, String in_description, String in_prezzo, String in_data_scad, String in_cene_da_vend) throws ParseException, FileNotFoundException, ClassNotFoundException, IOException
    {


                //System.out.println("---------- Nuova Offerta Cena ----------");
                //System.out.print("Ristorante: ");
                String ristorante = in_ristorante;

                //System.out.print("Localita: ");
                String localitaCena = in_localita;

                //System.out.print("Descrizione: ");
                String descrizioneCena = in_description;

                //System.out.print("Costo a Persona: ");
                String prezzoCena = in_prezzo;
                double prezzoPSingCena = Double.parseDouble(prezzoCena);

                //System.out.print("Data Scadenza offerta (GG/MM/AAAA): ");
                String data3 = in_data_scad;
                String temp3[];
                String marcatore2="/";
                temp3 = data3.split(marcatore2);
                GregorianCalendar dataScadCena = new GregorianCalendar((Integer.parseInt(temp3[2])),(Integer.parseInt(temp3[1]))-1,(Integer.parseInt(temp3[0])));

                //System.out.print("Cene da Vendere: ");
                String nCene = in_cene_da_vend;
                int ceneDaVendere = Integer.parseInt(nCene);

                catalogoOfferte.add(new CeneInRistoranti(Global.get_next_id(), ristorante, localitaCena, descrizioneCena, prezzoPSingCena, dataScadCena, ceneDaVendere));
                store_data();

        IdCounter.save_counter();
    }
    
    /**
     * Questo metodo aggiunge al catalogo un nuovo bene.
     * 
     * @param in_descrizione - descrizione del bene.
     * @param in_prezzo - prezzo del bene.
     * @param in_quantita -  quantit� dei beni da vendere.
     * @param in_fornitore - fornitore del ben.e
     * @param in_giudizio - giudizio del fornitore.
     * @throws ParseException
     * @throws FileNotFoundException
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public void nuovoProdottoBene(String in_descrizione, String in_prezzo, String in_quantita, String in_fornitore, String in_giudizio) throws ParseException, FileNotFoundException, ClassNotFoundException, IOException
    {


                //System.out.println("---------- Nuova Offerta Bene ----------");
                //System.out.print("Descrizione: ");
                String descrizioneBene = in_descrizione;

                //System.out.print("Prezzo Bene: ");
                String prezzoBene = in_prezzo;
                int prezzo_bene = Integer.parseInt(prezzoBene);

                //System.out.print("Quantita in magazzino: ");
                String qntMag = in_quantita;
                int qnt_mag = Integer.parseInt(prezzoBene);

                String fornitore = in_fornitore;

                String giudizio = in_giudizio;
                int giud_int = Integer.parseInt(giudizio);


                catalogoOfferte.add(new BeniDiConsumo(Global.get_next_id(), descrizioneBene, prezzo_bene, qnt_mag,fornitore,giud_int));
                store_data();

        IdCounter.save_counter();
    }
    
    /**
     * Questo metodo aggiunge una nuova prestazione d'opera all'interno del catalogo.
     * 
     * @param in_localita Localita' dove e' operativa tale prestazione.
     * @param in_descrizione Descrizione della prestazione.
     * @param in_fornitore Fornitore della prestazione.
     * @param in_giudizio Giudizio del fornitore.
     * @param in_prezzo Prezzo della prestazione.
     * @throws ParseException
     * @throws FileNotFoundException
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public void nuovoProdottoPrestazione(String in_localita, String in_descrizione, String in_fornitore, String in_giudizio, String in_prezzo) throws ParseException, FileNotFoundException, ClassNotFoundException, IOException
    {
                //System.out.println("---------- Nuova Offerta Prestazione ----------");

                String localita  = in_localita;
                String descrizione = in_descrizione;
                String fornitore = in_fornitore;


                String giudizio = in_giudizio;
                int giudizio_int = Integer.parseInt(giudizio);


                String prezzo = in_prezzo;
                double prezzo_d = Double.parseDouble(prezzo);

                catalogoOfferte.add(new PrestazioniDopera(Global.get_next_id(), localita,descrizione,fornitore,giudizio_int,prezzo_d));
                store_data();

        IdCounter.save_counter();
    }
	
	/**
	 * Questo metodo permette la cancellazione di un prodotto dal catalogo.
	 * 
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public boolean cancellaProdotto(String id) throws FileNotFoundException, ClassNotFoundException, IOException
	{
		//System.out.println("------ Lista Prodotti -----\n--");

		
		//System.out.print("Elimina prodotto [Inserisci id]: ");
		//Scanner inputData = new Scanner(System.in);

		int idDel = Integer.parseInt(id);
		//System.out.println(idDel);

		for(Utilizzabile deal: catalogoOfferte)
		{
			if(deal instanceof Vacanze && ((Vacanze)deal).getIdViaggio()==idDel)
			{
				catalogoOfferte.remove(deal);
				store_data();
				return true;
			}
			if(deal instanceof BeniDiConsumo && ((BeniDiConsumo) deal).getIdBene()==idDel)
			{
				catalogoOfferte.remove(deal);
				store_data();
                return true;
			}
			if(deal instanceof CeneInRistoranti && ((CeneInRistoranti) deal).getIdCena()==idDel)
			{
				catalogoOfferte.remove(deal);
				store_data();
                return true;
			}
            if(deal instanceof PrestazioniDopera && ((PrestazioniDopera)deal).getIdPrOpera()==idDel)
            {
                catalogoOfferte.remove(deal);
                store_data();
                return true;
            }
		}
        return false;
	}

	/**
	 * Questo metodo velocizza la stampa dell informazioni scorrenndo l'array.
	 * 
	 * @param a ArrayList da scorrere
	 * @param show_offers TexaArea dove visualizzare l'arraylist
	 */
    public void print_cat_admin (ArrayList<Utilizzabile> a,JTextArea show_offers){
        for(Utilizzabile deal: a)
        {
            if(deal instanceof Vacanze)
            {
                Vacanze dealVacanza = (Vacanze)deal;
                if((dealVacanza.eAcquistabile()))
                {
                    visualizzaVacanza(dealVacanza, show_offers);
                }
                continue;
            }
            if(deal instanceof BeniDiConsumo)
            {
                BeniDiConsumo dealBene = (BeniDiConsumo)deal;
                if((dealBene.eAcquistabile()))
                {
                    visualizzaBene(dealBene,show_offers);
                }
                continue;
            }
            if(deal instanceof CeneInRistoranti)
            {
                CeneInRistoranti dealCena = (CeneInRistoranti)deal;

                if((dealCena.eAcquistabile()))
                {
                    visualizzaCena(dealCena,show_offers);
                }
                continue;
            }
            if(deal instanceof PrestazioniDopera)
            {
                PrestazioniDopera dealP = (PrestazioniDopera)deal;
                if((deal.eAcquistabile()))
                {
                    visualizzaPrestazione(dealP,show_offers);
                }
                continue;
            }
        }
    }

   /**
    * Stampa la lista degli oggetti utilizzabile escludendo le cene con location diversa da quelle dell'utente.
    * 
    * @param a ArrayList da scorrere
    * @param location_user_to_compare La localita' dell'utente da confrontare con glia rticoli del catalogo
    * @param show_offers TexArea dove vsualizzare i risultati
    */
    public void print_cat_user (ArrayList<Utilizzabile> a, String location_user_to_compare, JTextArea show_offers){
        for(Utilizzabile deal: a)
        {
            if(deal instanceof Vacanze)
            {
                Vacanze dealVacanza = (Vacanze)deal;
                if((dealVacanza.eAcquistabile()))
                {
                    visualizzaVacanza(dealVacanza,show_offers);
                }
                continue;
            }
            if(deal instanceof BeniDiConsumo)
            {
                BeniDiConsumo dealBene = (BeniDiConsumo)deal;
                if((dealBene.eAcquistabile()))
                {
                    visualizzaBene(dealBene,show_offers);
                }
                continue;
            }
            if(deal instanceof CeneInRistoranti)
            {
                CeneInRistoranti dealCena = (CeneInRistoranti)deal;

                if((dealCena.eAcquistabile() && dealCena.getLuogo().equals(location_user_to_compare )))
                {
                    visualizzaCena(dealCena,show_offers);
                }
                continue;
            }
            if(deal instanceof PrestazioniDopera)
            {
                PrestazioniDopera dealP = (PrestazioniDopera)deal;
                if((deal.eAcquistabile()))
                {
                    visualizzaPrestazione(dealP, show_offers);
                }
                continue;
            }
        }
    }

    /**
     * Questo metodo converte una data in milli secondi.
     * 
     * @param in data da convertire in millisecondi
     * @return Il numero in millisencodi della data passata alla funzione.
     */
    protected long convert_calendar_to_int (GregorianCalendar in){
               return in.getTimeInMillis();
    }

	/**
	 * Questo metodo visualizza le offerte attive del catalogo. Viene differenziata la visualizzazione in base all'utente.
	 * 
	 * - L'amministratore avra' la possibilita' di scegliere l'ordinamento in base dalla data di scadenza o ID prodotti.
	 * - L'utente visualizzera' le offerte attive senza possibilita' di ordinamento.
	 * 
	 * @param user username utente
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void offerteAttive(String user, JTextArea show_offers, String in_choice) throws FileNotFoundException, ClassNotFoundException, IOException
	{
		Entry loggedUser = Access.get_user_data(user);
		if(!(loggedUser.getType()))
		{
            //admin
			
            if  (in_choice.equals("1")){

                class CompareDataScadenza implements Comparator<Utilizzabile> {

                    @Override
                    public int compare(Utilizzabile e1, Utilizzabile e2) {

                      if (e1 instanceof BeniDiConsumo){
                           //beni vs
                            if (e2 instanceof Vacanze){
                                //beni vs vacanze
                                if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                    return -1;
                                    return 1;

                            }else{
                                if (e2 instanceof BeniDiConsumo){
                                    //bene vs //bene

                                    if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                        return -1;
                                    return 1;

                                }else{
                                    //bene vs cena
                                    if (e2 instanceof CeneInRistoranti) {

                                        if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                            return -1;
                                        return 1;
                                    }else{

                                        //bene vs prest
                                        if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                            return -1;
                                        return 1;

                                    }
                                }
                            }
                      }else{
                          if (e1 instanceof Vacanze){
                               //vacanze vs
                              if (e2 instanceof BeniDiConsumo){
                                  //vacanze vs bene
                                  if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                      return -1;
                                  return 1;

                              }else{
                                  if (e2 instanceof Vacanze){
                                      //vacabnza vs vacanza

                                      if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                          return -1;
                                      return 1;


                                  }else{
                                      //vacanza vs cena

                                      if (e2 instanceof CeneInRistoranti) {

                                          if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                              return -1;
                                          return 1;
                                      }else{
                                          //vacanza vs prest
                                          if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                              return -1;
                                          return 1;

                                      }

                                  }
                              }

                          }else{
                              //Cene vs
                              if (e1 instanceof CeneInRistoranti){
                                      if (e2 instanceof Vacanze){
                                          //cena vs vacanza

                                          if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                              return -1;
                                          return 1;

                                      }else{
                                          if (e2 instanceof CeneInRistoranti){
                                              //cena vs cena

                                              if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                                  return -1;
                                              return 1;
                                          }else{
                                              //cena vs bene

                                              if (e2 instanceof BeniDiConsumo) {

                                                  if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                                      return -1;
                                                  return 1;
                                              }else{
                                                  //cena vs prest
                                                  if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                                      return -1;
                                                  return 1;

                                              }
                                          }
                                      }
                              }else{

                                  if (e2 instanceof Vacanze){
                                      //prest vs vacanza

                                      if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                          return -1;
                                      return 1;

                                  }else{
                                      if (e2 instanceof CeneInRistoranti){
                                          //prest vs cena

                                          if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                              return -1;
                                          return 1;
                                      }else{
                                          //prest vs bene

                                          if (e2 instanceof BeniDiConsumo) {

                                              if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                                  return -1;
                                              return 1;
                                          }else{
                                              //prest vs prest
                                              if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                                  return -1;
                                              return 1;

                                          }
                                      }
                                  }

                              }

                          }
                      }

                    }
                }

                Collections.sort(catalogoOfferte,new CompareDataScadenza());
                print_cat_admin (catalogoOfferte,show_offers);
            }else{

                class CompareID implements Comparator<Utilizzabile> {

                    @Override
                    public int compare(Utilizzabile e1, Utilizzabile e2) {

                        if (e1 instanceof BeniDiConsumo){
                            //beni vs
                            if (e2 instanceof Vacanze){
                                //beni vs vacanze
                                if (((BeniDiConsumo)e1).getIdBene() < ((Vacanze)e2).getIdViaggio())
                                    return -1;
                                return 1;

                            }else{
                                if (e2 instanceof BeniDiConsumo){
                                    //bene vs //bene

                                    if (((BeniDiConsumo)e1).getIdBene() < ((BeniDiConsumo)e2).getIdBene())
                                        return -1;
                                    return 1;

                                }else{
                                    //bene vs cena
                                    if (e2 instanceof CeneInRistoranti){

                                        if (((BeniDiConsumo)e1).getIdBene() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{

                                        if (((BeniDiConsumo)e1).getIdBene() < ((PrestazioniDopera)e2).getIdPrOpera())
                                            return -1;
                                        return 1;
                                    }
                                }
                            }
                        }else{
                            if (e1 instanceof Vacanze){
                                //vacanze vs
                                if (e2 instanceof BeniDiConsumo){
                                    //vacanze vs bene
                                    if (((Vacanze)e1).getIdViaggio() < ((BeniDiConsumo)e2).getIdBene())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof Vacanze){
                                        //vacabnza vs vacanza

                                        if (((Vacanze)e1).getIdViaggio() < ((Vacanze)e2).getIdViaggio())
                                            return -1;
                                        return 1;
                                        
                                    }else{
                                        //vacanza vs cena

                                        if (e2 instanceof CeneInRistoranti){

                                            if (((Vacanze)e1).getIdViaggio() < ((CeneInRistoranti)e2).getIdCena())
                                                return -1;
                                            return 1;
                                        }else{

                                            if (((Vacanze)e1).getIdViaggio() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                return -1;
                                            return 1;
                                        }

                                    }
                                }

                            }else{
                                if (e1 instanceof CeneInRistoranti){


                                    //Cene vs
                                    if (e2 instanceof Vacanze){
                                        //cena vs vacanza

                                        if (((CeneInRistoranti)e1).getIdCena() < ((Vacanze)e2).getIdViaggio())
                                            return -1;
                                        return 1;

                                    }else{
                                        if (e2 instanceof CeneInRistoranti){
                                            //cena vs cena

                                            if (((CeneInRistoranti)e1).getIdCena() < ((CeneInRistoranti)e2).getIdCena())
                                                return -1;
                                            return 1;
                                        }else{
                                            //cena vs bene

                                            if (e2 instanceof BeniDiConsumo){

                                                if (((CeneInRistoranti)e1).getIdCena() < ((BeniDiConsumo)e2).getIdBene())
                                                    return -1;
                                                return 1;
                                            }else{

                                                if (((CeneInRistoranti)e1).getIdCena() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                    return -1;
                                                return 1;
                                            }
                                        }
                                    }
                                }else{

                                    if (e2 instanceof Vacanze){
                                        //prest vs vacanza

                                        if (((PrestazioniDopera)e1).getIdPrOpera() < ((Vacanze)e2).getIdViaggio())
                                            return -1;
                                        return 1;

                                    }else{
                                        if (e2 instanceof CeneInRistoranti){
                                            //prest vs cena

                                            if (((PrestazioniDopera)e1).getIdPrOpera() < ((CeneInRistoranti)e2).getIdCena())
                                                return -1;
                                            return 1;
                                        }else{
                                            //prest vs bene

                                            if (e2 instanceof BeniDiConsumo){

                                                if (((PrestazioniDopera)e1).getIdPrOpera() < ((BeniDiConsumo)e2).getIdBene())
                                                    return -1;
                                                return 1;
                                            }else{

                                                if (((PrestazioniDopera)e1).getIdPrOpera() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                    return -1;
                                                return 1;
                                            }
                                        }
                                    }

                                }

                            }
                        }

                    }
                }

                Collections.sort(catalogoOfferte,new CompareID());
                print_cat_admin (catalogoOfferte,show_offers);
            }

		} else{

            // user

            class CompareIDUser implements Comparator<Utilizzabile> {

                @Override
                public int compare(Utilizzabile e1, Utilizzabile e2) {

                    if (e1 instanceof BeniDiConsumo){
                        //beni vs
                        if (e2 instanceof Vacanze){
                            //beni vs vacanze
                            if (((BeniDiConsumo)e1).getIdBene() < ((Vacanze)e2).getIdViaggio())
                                return -1;
                            return 1;

                        }else{
                            if (e2 instanceof BeniDiConsumo){
                                //bene vs //bene

                                if (((BeniDiConsumo)e1).getIdBene() < ((BeniDiConsumo)e2).getIdBene())
                                    return -1;
                                return 1;

                            }else{
                                //bene vs cena
                                if (e2 instanceof CeneInRistoranti){

                                    if (((BeniDiConsumo)e1).getIdBene() < ((CeneInRistoranti)e2).getIdCena())
                                        return -1;
                                    return 1;
                                }else{

                                    if (((BeniDiConsumo)e1).getIdBene() < ((PrestazioniDopera)e2).getIdPrOpera())
                                        return -1;
                                    return 1;
                                }
                            }
                        }
                    }else{
                        if (e1 instanceof Vacanze){
                            //vacanze vs
                            if (e2 instanceof BeniDiConsumo){
                                //vacanze vs bene
                                if (((Vacanze)e1).getIdViaggio() < ((BeniDiConsumo)e2).getIdBene())
                                    return -1;
                                return 1;

                            }else{
                                if (e2 instanceof Vacanze){
                                    //vacabnza vs vacanza

                                    if (((Vacanze)e1).getIdViaggio() < ((Vacanze)e2).getIdViaggio())
                                        return -1;
                                    return 1;


                                }else{
                                    //vacanza vs cena

                                    if (e2 instanceof CeneInRistoranti){

                                        if (((Vacanze)e1).getIdViaggio() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{

                                        if (((Vacanze)e1).getIdViaggio() < ((PrestazioniDopera)e2).getIdPrOpera())
                                            return -1;
                                        return 1;
                                    }

                                }
                            }

                        }else{
                            if (e1 instanceof CeneInRistoranti){


                            //Cene vs
                                if (e2 instanceof Vacanze){
                                    //cena vs vacanza

                                    if (((CeneInRistoranti)e1).getIdCena() < ((Vacanze)e2).getIdViaggio())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof CeneInRistoranti){
                                        //cena vs cena

                                        if (((CeneInRistoranti)e1).getIdCena() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{
                                        //cena vs bene

                                        if (e2 instanceof BeniDiConsumo){

                                            if (((CeneInRistoranti)e1).getIdCena() < ((BeniDiConsumo)e2).getIdBene())
                                                return -1;
                                            return 1;
                                        }else{

                                            if (((CeneInRistoranti)e1).getIdCena() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                return -1;
                                            return 1;
                                        }
                                    }
                                }
                            }else{

                                if (e2 instanceof Vacanze){
                                    //prest vs vacanza

                                    if (((PrestazioniDopera)e1).getIdPrOpera() < ((Vacanze)e2).getIdViaggio())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof CeneInRistoranti){
                                        //prest vs cena

                                        if (((PrestazioniDopera)e1).getIdPrOpera() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{
                                        //prest vs bene

                                        if (e2 instanceof BeniDiConsumo){

                                            if (((PrestazioniDopera)e1).getIdPrOpera() < ((BeniDiConsumo)e2).getIdBene())
                                                return -1;
                                            return 1;
                                        }else{

                                            if (((PrestazioniDopera)e1).getIdPrOpera() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                return -1;
                                            return 1;
                                        }
                                    }
                                }

                            }

                        }
                    }

                }
            }

            Collections.sort(catalogoOfferte, new CompareIDUser());
            print_cat_user(catalogoOfferte, loggedUser.getLocation(), show_offers);
        }
	}

	/**
	 * Questo metodo genera una lista di offerte attive comprese tra una data iniziale ed una finale
	 * 
	 * @param user Utente connesso 
	 * @param show_offers TextArea dove mostrare i risultati
	 * @param in_choice scelta 
	 * @param datastart data di inizio della ricerca
	 * @param data_end data finale della ricerca
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
    public void offerteAttiveConScadenza(String user, JTextArea show_offers, String in_choice, GregorianCalendar datastart, GregorianCalendar data_end) throws FileNotFoundException, ClassNotFoundException, IOException
    {
        Entry loggedUser = Access.get_user_data(user);
        ArrayList<Utilizzabile> arrayData = getDataExpire(datastart,data_end);
        if(!(loggedUser.getType()))
        {
            //admin

            if  (in_choice.equals("1")){
                class CompareDataScadenza implements Comparator<Utilizzabile> {

                    @Override
                    public int compare(Utilizzabile e1, Utilizzabile e2) {

                        if (e1 instanceof BeniDiConsumo){
                            //beni vs
                            if (e2 instanceof Vacanze){
                                //beni vs vacanze
                                if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                    return -1;
                                return 1;

                            }else{
                                if (e2 instanceof BeniDiConsumo){
                                    //bene vs //bene

                                    if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                        return -1;
                                    return 1;

                                }else{
                                    //bene vs cena
                                    if (e2 instanceof CeneInRistoranti) {

                                        if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                            return -1;
                                        return 1;
                                    }else{

                                        //bene vs prest
                                        if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                            return -1;
                                        return 1;

                                    }
                                }
                            }
                        }else{
                            if (e1 instanceof Vacanze){
                                //vacanze vs
                                if (e2 instanceof BeniDiConsumo){
                                    //vacanze vs bene
                                    if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof Vacanze){
                                        //vacabnza vs vacanza

                                        if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                            return -1;
                                        return 1;


                                    }else{
                                        //vacanza vs cena

                                        if (e2 instanceof CeneInRistoranti) {

                                            if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                                return -1;
                                            return 1;
                                        }else{
                                            //vacanza vs prest
                                            if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                                return -1;
                                            return 1;

                                        }

                                    }
                                }

                            }else{
                                //Cene vs
                                if (e1 instanceof CeneInRistoranti){
                                    if (e2 instanceof Vacanze){
                                        //cena vs vacanza

                                        if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                            return -1;
                                        return 1;

                                    }else{
                                        if (e2 instanceof CeneInRistoranti){
                                            //cena vs cena

                                            if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                                return -1;
                                            return 1;
                                        }else{
                                            //cena vs bene

                                            if (e2 instanceof BeniDiConsumo) {

                                                if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                                    return -1;
                                                return 1;
                                            }else{
                                                //cena vs prest
                                                if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                                    return -1;
                                                return 1;

                                            }
                                        }
                                    }
                                }else{

                                    if (e2 instanceof Vacanze){
                                        //prest vs vacanza

                                        if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                            return -1;
                                        return 1;

                                    }else{
                                        if (e2 instanceof CeneInRistoranti){
                                            //prest vs cena

                                            if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                                return -1;
                                            return 1;
                                        }else{
                                            //prest vs bene

                                            if (e2 instanceof BeniDiConsumo) {

                                                if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                                    return -1;
                                                return 1;
                                            }else{
                                                //prest vs prest
                                                if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                                    return -1;
                                                return 1;

                                            }
                                        }
                                    }

                                }

                            }
                        }

                    }
                }

                Collections.sort(arrayData,new CompareDataScadenza());
                print_cat_admin (arrayData,show_offers);
            }else{

                class CompareID implements Comparator<Utilizzabile> {

                    @Override
                    public int compare(Utilizzabile e1, Utilizzabile e2) {

                        if (e1 instanceof BeniDiConsumo){
                            //beni vs
                            if (e2 instanceof Vacanze){
                                //beni vs vacanze
                                if (((BeniDiConsumo)e1).getIdBene() < ((Vacanze)e2).getIdViaggio())
                                    return -1;
                                return 1;

                            }else{
                                if (e2 instanceof BeniDiConsumo){
                                    //bene vs //bene

                                    if (((BeniDiConsumo)e1).getIdBene() < ((BeniDiConsumo)e2).getIdBene())
                                        return -1;
                                    return 1;

                                }else{
                                    //bene vs cena
                                    if (e2 instanceof CeneInRistoranti){

                                        if (((BeniDiConsumo)e1).getIdBene() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{

                                        if (((BeniDiConsumo)e1).getIdBene() < ((PrestazioniDopera)e2).getIdPrOpera())
                                            return -1;
                                        return 1;
                                    }
                                }
                            }
                        }else{
                            if (e1 instanceof Vacanze){
                                //vacanze vs
                                if (e2 instanceof BeniDiConsumo){
                                    //vacanze vs bene
                                    if (((Vacanze)e1).getIdViaggio() < ((BeniDiConsumo)e2).getIdBene())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof Vacanze){
                                        //vacabnza vs vacanza

                                        if (((Vacanze)e1).getIdViaggio() < ((Vacanze)e2).getIdViaggio())
                                            return -1;
                                        return 1;


                                    }else{
                                        //vacanza vs cena

                                        if (e2 instanceof CeneInRistoranti){

                                            if (((Vacanze)e1).getIdViaggio() < ((CeneInRistoranti)e2).getIdCena())
                                                return -1;
                                            return 1;
                                        }else{

                                            if (((Vacanze)e1).getIdViaggio() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                return -1;
                                            return 1;
                                        }

                                    }
                                }

                            }else{
                                if (e1 instanceof CeneInRistoranti){


                                    //Cene vs
                                    if (e2 instanceof Vacanze){
                                        //cena vs vacanza

                                        if (((CeneInRistoranti)e1).getIdCena() < ((Vacanze)e2).getIdViaggio())
                                            return -1;
                                        return 1;

                                    }else{
                                        if (e2 instanceof CeneInRistoranti){
                                            //cena vs cena

                                            if (((CeneInRistoranti)e1).getIdCena() < ((CeneInRistoranti)e2).getIdCena())
                                                return -1;
                                            return 1;
                                        }else{
                                            //cena vs bene

                                            if (e2 instanceof BeniDiConsumo){

                                                if (((CeneInRistoranti)e1).getIdCena() < ((BeniDiConsumo)e2).getIdBene())
                                                    return -1;
                                                return 1;
                                            }else{

                                                if (((CeneInRistoranti)e1).getIdCena() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                    return -1;
                                                return 1;
                                            }
                                        }
                                    }
                                }else{

                                    if (e2 instanceof Vacanze){
                                        //prest vs vacanza

                                        if (((PrestazioniDopera)e1).getIdPrOpera() < ((Vacanze)e2).getIdViaggio())
                                            return -1;
                                        return 1;

                                    }else{
                                        if (e2 instanceof CeneInRistoranti){
                                            //prest vs cena

                                            if (((PrestazioniDopera)e1).getIdPrOpera() < ((CeneInRistoranti)e2).getIdCena())
                                                return -1;
                                            return 1;
                                        }else{
                                            //prest vs bene

                                            if (e2 instanceof BeniDiConsumo){

                                                if (((PrestazioniDopera)e1).getIdPrOpera() < ((BeniDiConsumo)e2).getIdBene())
                                                    return -1;
                                                return 1;
                                            }else{

                                                if (((PrestazioniDopera)e1).getIdPrOpera() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                    return -1;
                                                return 1;
                                            }
                                        }
                                    }

                                }

                            }
                        }

                    }
                }

                Collections.sort(arrayData,new CompareID());
                print_cat_admin (arrayData,show_offers);
            }

        } else{

            // user

            class CompareIDUser implements Comparator<Utilizzabile> {

                @Override
                public int compare(Utilizzabile e1, Utilizzabile e2) {

                    if (e1 instanceof BeniDiConsumo){
                        //beni vs
                        if (e2 instanceof Vacanze){
                            //beni vs vacanze
                            if (((BeniDiConsumo)e1).getIdBene() < ((Vacanze)e2).getIdViaggio())
                                return -1;
                            return 1;

                        }else{
                            if (e2 instanceof BeniDiConsumo){
                                //bene vs //bene

                                if (((BeniDiConsumo)e1).getIdBene() < ((BeniDiConsumo)e2).getIdBene())
                                    return -1;
                                return 1;

                            }else{
                                //bene vs cena
                                if (e2 instanceof CeneInRistoranti){

                                    if (((BeniDiConsumo)e1).getIdBene() < ((CeneInRistoranti)e2).getIdCena())
                                        return -1;
                                    return 1;
                                }else{

                                    if (((BeniDiConsumo)e1).getIdBene() < ((PrestazioniDopera)e2).getIdPrOpera())
                                        return -1;
                                    return 1;
                                }
                            }
                        }
                    }else{
                        if (e1 instanceof Vacanze){
                            //vacanze vs
                            if (e2 instanceof BeniDiConsumo){
                                //vacanze vs bene
                                if (((Vacanze)e1).getIdViaggio() < ((BeniDiConsumo)e2).getIdBene())
                                    return -1;
                                return 1;

                            }else{
                                if (e2 instanceof Vacanze){
                                    //vacabnza vs vacanza

                                    if (((Vacanze)e1).getIdViaggio() < ((Vacanze)e2).getIdViaggio())
                                        return -1;
                                    return 1;


                                }else{
                                    //vacanza vs cena

                                    if (e2 instanceof CeneInRistoranti){

                                        if (((Vacanze)e1).getIdViaggio() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{

                                        if (((Vacanze)e1).getIdViaggio() < ((PrestazioniDopera)e2).getIdPrOpera())
                                            return -1;
                                        return 1;
                                    }

                                }
                            }

                        }else{
                            if (e1 instanceof CeneInRistoranti){


                                //Cene vs
                                if (e2 instanceof Vacanze){
                                    //cena vs vacanza

                                    if (((CeneInRistoranti)e1).getIdCena() < ((Vacanze)e2).getIdViaggio())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof CeneInRistoranti){
                                        //cena vs cena

                                        if (((CeneInRistoranti)e1).getIdCena() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{
                                        //cena vs bene

                                        if (e2 instanceof BeniDiConsumo){

                                            if (((CeneInRistoranti)e1).getIdCena() < ((BeniDiConsumo)e2).getIdBene())
                                                return -1;
                                            return 1;
                                        }else{

                                            if (((CeneInRistoranti)e1).getIdCena() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                return -1;
                                            return 1;
                                        }
                                    }
                                }
                            }else{

                                if (e2 instanceof Vacanze){
                                    //prest vs vacanza

                                    if (((PrestazioniDopera)e1).getIdPrOpera() < ((Vacanze)e2).getIdViaggio())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof CeneInRistoranti){
                                        //prest vs cena

                                        if (((PrestazioniDopera)e1).getIdPrOpera() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{
                                        //prest vs bene

                                        if (e2 instanceof BeniDiConsumo){

                                            if (((PrestazioniDopera)e1).getIdPrOpera() < ((BeniDiConsumo)e2).getIdBene())
                                                return -1;
                                            return 1;
                                        }else{

                                            if (((PrestazioniDopera)e1).getIdPrOpera() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                return -1;
                                            return 1;
                                        }
                                    }
                                }

                            }

                        }
                    }

                }
            }

            Collections.sort(arrayData, new CompareIDUser());
            print_cat_user(arrayData, loggedUser.getLocation(), show_offers);
        }
    }
    
    /**
     * Questo metodo visualizza gli articoli senza scadenza che sono offerti da fornitori con un giudizio superiore ad un valore preso come parametro.
     * 
     * @param user Utente connesso 
     * @param show_offers TextArea dove mostrare i risultati
     * @param in_choice scelta
     * @param votazione votazione che i prodotti devono rispettare.
     * @throws FileNotFoundException
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public void offerteAttiveSenzaScadenza(String user, JTextArea show_offers, String in_choice, int votazione) throws FileNotFoundException, ClassNotFoundException, IOException
    {
        Entry loggedUser = Access.get_user_data(user);
        ArrayList<Utilizzabile> arrayData = getDealNotExpire(votazione);
        if(!(loggedUser.getType()))
        {
            //admin

            if  (in_choice.equals("1")){

                class CompareDataScadenza implements Comparator<Utilizzabile> {

                    @Override
                    public int compare(Utilizzabile e1, Utilizzabile e2) {

                        if (e1 instanceof BeniDiConsumo){
                            //beni vs
                            if (e2 instanceof Vacanze){
                                //beni vs vacanze
                                if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                    return -1;
                                return 1;

                            }else{
                                if (e2 instanceof BeniDiConsumo){
                                    //bene vs //bene

                                    if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                        return -1;
                                    return 1;

                                }else{
                                    //bene vs cena
                                    if (e2 instanceof CeneInRistoranti) {

                                        if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                            return -1;
                                        return 1;
                                    }else{

                                        //bene vs prest
                                        if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                            return -1;
                                        return 1;

                                    }
                                }
                            }
                        }else{
                            if (e1 instanceof Vacanze){
                                //vacanze vs
                                if (e2 instanceof BeniDiConsumo){
                                    //vacanze vs bene
                                    if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof Vacanze){
                                        //vacabnza vs vacanza

                                        if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                            return -1;
                                        return 1;


                                    }else{
                                        //vacanza vs cena

                                        if (e2 instanceof CeneInRistoranti) {

                                            if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                                return -1;
                                            return 1;
                                        }else{
                                            //vacanza vs prest
                                            if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                                return -1;
                                            return 1;

                                        }

                                    }
                                }

                            }else{
                                //Cene vs
                                if (e1 instanceof CeneInRistoranti){
                                    if (e2 instanceof Vacanze){
                                        //cena vs vacanza

                                        if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                            return -1;
                                        return 1;

                                    }else{
                                        if (e2 instanceof CeneInRistoranti){
                                            //cena vs cena

                                            if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                                return -1;
                                            return 1;
                                        }else{
                                            //cena vs bene

                                            if (e2 instanceof BeniDiConsumo) {

                                                if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                                    return -1;
                                                return 1;
                                            }else{
                                                //cena vs prest
                                                if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                                    return -1;
                                                return 1;

                                            }
                                        }
                                    }
                                }else{

                                    if (e2 instanceof Vacanze){
                                        //prest vs vacanza

                                        if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                            return -1;
                                        return 1;

                                    }else{
                                        if (e2 instanceof CeneInRistoranti){
                                            //prest vs cena

                                            if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                                return -1;
                                            return 1;
                                        }else{
                                            //prest vs bene

                                            if (e2 instanceof BeniDiConsumo) {

                                                if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                                    return -1;
                                                return 1;
                                            }else{
                                                //prest vs prest
                                                if (((PrestazioniDopera)e1).getScadenza().getTimeInMillis() < ((PrestazioniDopera)e2).getScadenza().getTimeInMillis())
                                                    return -1;
                                                return 1;

                                            }
                                        }
                                    }

                                }

                            }
                        }

                    }
                }

                Collections.sort(arrayData,new CompareDataScadenza());
                print_cat_admin (arrayData,show_offers);
            }else{

                class CompareID implements Comparator<Utilizzabile> {

                    @Override
                    public int compare(Utilizzabile e1, Utilizzabile e2) {

                        if (e1 instanceof BeniDiConsumo){
                            //beni vs
                            if (e2 instanceof Vacanze){
                                //beni vs vacanze
                                if (((BeniDiConsumo)e1).getIdBene() < ((Vacanze)e2).getIdViaggio())
                                    return -1;
                                return 1;

                            }else{
                                if (e2 instanceof BeniDiConsumo){
                                    //bene vs //bene

                                    if (((BeniDiConsumo)e1).getIdBene() < ((BeniDiConsumo)e2).getIdBene())
                                        return -1;
                                    return 1;

                                }else{
                                    //bene vs cena
                                    if (e2 instanceof CeneInRistoranti){

                                        if (((BeniDiConsumo)e1).getIdBene() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{

                                        if (((BeniDiConsumo)e1).getIdBene() < ((PrestazioniDopera)e2).getIdPrOpera())
                                            return -1;
                                        return 1;
                                    }
                                }
                            }
                        }else{
                            if (e1 instanceof Vacanze){
                                //vacanze vs
                                if (e2 instanceof BeniDiConsumo){
                                    //vacanze vs bene
                                    if (((Vacanze)e1).getIdViaggio() < ((BeniDiConsumo)e2).getIdBene())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof Vacanze){
                                        //vacabnza vs vacanza

                                        if (((Vacanze)e1).getIdViaggio() < ((Vacanze)e2).getIdViaggio())
                                            return -1;
                                        return 1;


                                    }else{
                                        //vacanza vs cena

                                        if (e2 instanceof CeneInRistoranti){

                                            if (((Vacanze)e1).getIdViaggio() < ((CeneInRistoranti)e2).getIdCena())
                                                return -1;
                                            return 1;
                                        }else{

                                            if (((Vacanze)e1).getIdViaggio() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                return -1;
                                            return 1;
                                        }

                                    }
                                }

                            }else{
                                if (e1 instanceof CeneInRistoranti){


                                    //Cene vs
                                    if (e2 instanceof Vacanze){
                                        //cena vs vacanza

                                        if (((CeneInRistoranti)e1).getIdCena() < ((Vacanze)e2).getIdViaggio())
                                            return -1;
                                        return 1;

                                    }else{
                                        if (e2 instanceof CeneInRistoranti){
                                            //cena vs cena

                                            if (((CeneInRistoranti)e1).getIdCena() < ((CeneInRistoranti)e2).getIdCena())
                                                return -1;
                                            return 1;
                                        }else{
                                            //cena vs bene

                                            if (e2 instanceof BeniDiConsumo){

                                                if (((CeneInRistoranti)e1).getIdCena() < ((BeniDiConsumo)e2).getIdBene())
                                                    return -1;
                                                return 1;
                                            }else{

                                                if (((CeneInRistoranti)e1).getIdCena() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                    return -1;
                                                return 1;
                                            }
                                        }
                                    }
                                }else{

                                    if (e2 instanceof Vacanze){
                                        //prest vs vacanza

                                        if (((PrestazioniDopera)e1).getIdPrOpera() < ((Vacanze)e2).getIdViaggio())
                                            return -1;
                                        return 1;

                                    }else{
                                        if (e2 instanceof CeneInRistoranti){
                                            //prest vs cena

                                            if (((PrestazioniDopera)e1).getIdPrOpera() < ((CeneInRistoranti)e2).getIdCena())
                                                return -1;
                                            return 1;
                                        }else{
                                            //prest vs bene

                                            if (e2 instanceof BeniDiConsumo){

                                                if (((PrestazioniDopera)e1).getIdPrOpera() < ((BeniDiConsumo)e2).getIdBene())
                                                    return -1;
                                                return 1;
                                            }else{

                                                if (((PrestazioniDopera)e1).getIdPrOpera() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                    return -1;
                                                return 1;
                                            }
                                        }
                                    }

                                }

                            }
                        }

                    }
                }


                Collections.sort(arrayData,new CompareID());
                print_cat_admin (arrayData,show_offers);

            }


        } else{

            // user

            class CompareIDUser implements Comparator<Utilizzabile> {

                @Override
                public int compare(Utilizzabile e1, Utilizzabile e2) {

                    if (e1 instanceof BeniDiConsumo){
                        //beni vs
                        if (e2 instanceof Vacanze){
                            //beni vs vacanze
                            if (((BeniDiConsumo)e1).getIdBene() < ((Vacanze)e2).getIdViaggio())
                                return -1;
                            return 1;

                        }else{
                            if (e2 instanceof BeniDiConsumo){
                                //bene vs //bene

                                if (((BeniDiConsumo)e1).getIdBene() < ((BeniDiConsumo)e2).getIdBene())
                                    return -1;
                                return 1;

                            }else{
                                //bene vs cena
                                if (e2 instanceof CeneInRistoranti){

                                    if (((BeniDiConsumo)e1).getIdBene() < ((CeneInRistoranti)e2).getIdCena())
                                        return -1;
                                    return 1;
                                }else{

                                    if (((BeniDiConsumo)e1).getIdBene() < ((PrestazioniDopera)e2).getIdPrOpera())
                                        return -1;
                                    return 1;
                                }
                            }
                        }
                    }else{
                        if (e1 instanceof Vacanze){
                            //vacanze vs
                            if (e2 instanceof BeniDiConsumo){
                                //vacanze vs bene
                                if (((Vacanze)e1).getIdViaggio() < ((BeniDiConsumo)e2).getIdBene())
                                    return -1;
                                return 1;

                            }else{
                                if (e2 instanceof Vacanze){
                                    //vacabnza vs vacanza

                                    if (((Vacanze)e1).getIdViaggio() < ((Vacanze)e2).getIdViaggio())
                                        return -1;
                                    return 1;


                                }else{
                                    //vacanza vs cena

                                    if (e2 instanceof CeneInRistoranti){

                                        if (((Vacanze)e1).getIdViaggio() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{

                                        if (((Vacanze)e1).getIdViaggio() < ((PrestazioniDopera)e2).getIdPrOpera())
                                            return -1;
                                        return 1;
                                    }

                                }
                            }

                        }else{
                            if (e1 instanceof CeneInRistoranti){


                                //Cene vs
                                if (e2 instanceof Vacanze){
                                    //cena vs vacanza

                                    if (((CeneInRistoranti)e1).getIdCena() < ((Vacanze)e2).getIdViaggio())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof CeneInRistoranti){
                                        //cena vs cena

                                        if (((CeneInRistoranti)e1).getIdCena() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{
                                        //cena vs bene

                                        if (e2 instanceof BeniDiConsumo){

                                            if (((CeneInRistoranti)e1).getIdCena() < ((BeniDiConsumo)e2).getIdBene())
                                                return -1;
                                            return 1;
                                        }else{

                                            if (((CeneInRistoranti)e1).getIdCena() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                return -1;
                                            return 1;
                                        }
                                    }
                                }
                            }else{

                                if (e2 instanceof Vacanze){
                                    //prest vs vacanza

                                    if (((PrestazioniDopera)e1).getIdPrOpera() < ((Vacanze)e2).getIdViaggio())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof CeneInRistoranti){
                                        //prest vs cena

                                        if (((PrestazioniDopera)e1).getIdPrOpera() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{
                                        //prest vs bene

                                        if (e2 instanceof BeniDiConsumo){

                                            if (((PrestazioniDopera)e1).getIdPrOpera() < ((BeniDiConsumo)e2).getIdBene())
                                                return -1;
                                            return 1;
                                        }else{

                                            if (((PrestazioniDopera)e1).getIdPrOpera() < ((PrestazioniDopera)e2).getIdPrOpera())
                                                return -1;
                                            return 1;
                                        }
                                    }
                                }

                            }

                        }
                    }

                }
            }

            Collections.sort(arrayData, new CompareIDUser());
            print_cat_user(arrayData, loggedUser.getLocation(), show_offers);
        }
    }
	
	/**
	 * Questo metodo visualizza le offerte scadute del catalogo.
	 * 
	 * @param user Utente connesso
	 * @param in_area TextArea dove visualizzare i risultati.
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void offerteScadute(String user,JTextArea in_area) throws FileNotFoundException, ClassNotFoundException, IOException
	{
		Entry loggedUser = Access.get_user_data(user);
		//System.out.println("------ Offerte Scadute -----");
		for(Utilizzabile deal: catalogoOfferte)
		{
			if(deal instanceof Vacanze)
			{
				Vacanze dealVacanza = (Vacanze)deal;
				if(!(dealVacanza.eAcquistabile())) {visualizzaVacanza(dealVacanza,in_area);}continue;
			}
			if(deal instanceof BeniDiConsumo)
			{
				BeniDiConsumo dealBene = (BeniDiConsumo)deal;
				if(!(dealBene.eAcquistabile())) {visualizzaBene(dealBene,in_area);}continue;
			}
			if(deal instanceof CeneInRistoranti)
			{
				CeneInRistoranti dealCena = (CeneInRistoranti)deal;
				if(!(dealCena.eAcquistabile())) {visualizzaCena(dealCena,in_area);}continue;
			}
            if(deal instanceof PrestazioniDopera)
            {
                PrestazioniDopera dealP = (PrestazioniDopera)deal;
                if(!(dealP.eAcquistabile())) {visualizzaPrestazione(dealP,in_area);}continue;
            }
		}
	}
	
	/**
	 * Questa metodo aggiunge credito al conto dell'utente
	 * 
	 * @param user - Utente connesso al sistema-
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void aggiungiCredito(String user, String in_double) throws FileNotFoundException, ClassNotFoundException, IOException
	{
		Entry loggedUser = Access.get_user_data(user);

		String euro = in_double;
		loggedUser.setBalance(Double.parseDouble(euro));
		Access.replace_data(user, loggedUser);
	}

	/**
	 * Questo metodo permette all'utente di effettuare un acquisto.
	 * 
	 * @param user Utente connesso
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public boolean acquistaProdotti(String user, ArrayList<Utilizzabile> Carrello) throws FileNotFoundException, ClassNotFoundException, IOException
	{
		Entry loggedUser = Access.get_user_data(user);
		ArrayList<StoricoItem> storico = loggedUser.getStorico();

        double totale = 0;
		
		for(Utilizzabile dealsc: Carrello)
		{
			if(dealsc instanceof Vacanze)
			{
				Vacanze dealVacanza = (Vacanze)dealsc;
				totale += dealVacanza.get_prezzo_scontato();
			}
			if(dealsc instanceof BeniDiConsumo)
			{
				BeniDiConsumo dealBene = (BeniDiConsumo)dealsc;
                totale += dealBene.get_prezzo_scontato();
				
			}
			if( dealsc instanceof CeneInRistoranti)
			{
				CeneInRistoranti dealCena = (CeneInRistoranti)dealsc;
                totale += dealCena.get_prezzo_scontato();
			}

            if( dealsc instanceof PrestazioniDopera)
            {
                PrestazioniDopera deal = (PrestazioniDopera)dealsc;
                totale += deal.get_prezzo_scontato();
            }
		}

        if (Carrello.size() >= Global.getMin_num_articoli_per_sconto()){
            totale = totale - (totale * Global.getSconto_per_min_num_articoli());
        }

        if (totale <= loggedUser.getBalance()){

            for(Utilizzabile dealsc: Carrello)
            {
                if(dealsc instanceof Vacanze)
                {
                    Vacanze dealVacanza = (Vacanze)dealsc;

                        storico.add(new StoricoItem("Viaggio: "+dealVacanza.getLocalitaViaggio(), new GregorianCalendar() ,dealVacanza.get_prezzo_scontato()));
                        dealVacanza.setViaggiVenduti();
                        store_data();
                        Access.replace_data(user, loggedUser);

                }
                if(dealsc instanceof BeniDiConsumo)
                {
                    BeniDiConsumo dealBene = (BeniDiConsumo)dealsc;

                        storico.add(new StoricoItem("Prodotto: "+dealBene.getDescrizioneBene(), new GregorianCalendar() ,dealBene.get_prezzo_scontato()));
                        dealBene.setBeniVenduti();
                        Access.replace_data(user, loggedUser);
                        store_data();



                }
                if (dealsc instanceof CeneInRistoranti)
                {
                    CeneInRistoranti dealCena = (CeneInRistoranti)dealsc;

                        storico.add(new StoricoItem("Cena ristorante: "+dealCena.getNomeRistorante()+" "+dealCena.getDescrizione_cena(), new GregorianCalendar() ,dealCena.get_prezzo_scontato()));
                        dealCena.setCeneVendute();
                        Access.replace_data(user, loggedUser);
                        store_data();

                }
                if (dealsc instanceof PrestazioniDopera)
                {
                    PrestazioniDopera deal = (PrestazioniDopera)dealsc;

                    storico.add(new StoricoItem("Prestazione d'opera: "+deal.getDescrizione(), new GregorianCalendar() ,deal.get_prezzo_scontato()));
                    deal.setPrestazioniVendute();
                    Access.replace_data(user, loggedUser);
                    store_data();
                }
            }

            loggedUser.setBalance(-totale);
            Carrello.clear();
            Access.replace_data(user, loggedUser);
            store_data();

            return true;
        }else{
            return false;
        }
	}

	/**
	 * Metodo che permette la ricerca di un prodotto all'interno del catalogo in base all'id.
	 * 
	 * @param user Utente
	 * @param in_id id del prodotto da ricercare
	 * @return L'indice all'interno del catalogo.
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
    public int search_product_id(String user, String in_id) throws FileNotFoundException, ClassNotFoundException, IOException
    {
        Entry loggedUser = Access.get_user_data(user);
        //System.out.println("----- Shop ----");
        //System.out.println("### Prodotti ###");
 

        //System.out.println("Offerta da acquistare[inserisci id]: ");
        String idProd = in_id;
        int idDel = Integer.parseInt(idProd);

        int trovato = -1;

        for( int i= 0; i< catalogoOfferte.size() ;i++)
        {
            Utilizzabile dealsc = catalogoOfferte.get(i);
            if((dealsc instanceof Vacanze) && ((Vacanze)dealsc).getIdViaggio()==idDel)
            {
                Vacanze dealVacanza = (Vacanze)dealsc;
                if(dealVacanza.eAcquistabile() )
                {
                    trovato=i;
                }
                else
                {
                    break;
                }
            }
            if((dealsc instanceof BeniDiConsumo) && ((BeniDiConsumo) dealsc).getIdBene()==idDel)
            {
                BeniDiConsumo dealBene = (BeniDiConsumo)dealsc;
                if(dealBene.eAcquistabile() )
                {
                    trovato=i;

                }
                else
                {

                    break;
                }

            }
            if( (dealsc instanceof CeneInRistoranti) && ((CeneInRistoranti) dealsc).getIdCena()==idDel)
            {
                CeneInRistoranti dealCena = (CeneInRistoranti)dealsc;
                if(dealCena.eAcquistabile() )
                {
                    trovato=i;
                                    }
                else
                {
                    break;
                }
            }

            if( (dealsc instanceof PrestazioniDopera) && ((PrestazioniDopera) dealsc).getIdPrOpera()==idDel)
            {
                PrestazioniDopera deal = (PrestazioniDopera)dealsc;
                if(deal.eAcquistabile() )
                {
                    trovato=i;
                                    }
                else
                {

                    break;
                }
            }
        }
        return trovato;
    }

    /**
	 * Questo metodo visualizza lo storico degli ordini effettuati dall'utente. rende possibile l'ordinamento in base alla data di scadenza o per prezzo.
	 * 
	 * @param user - Utente
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void visualizzaStorico(String user, String choice,JTextArea area_storico) throws FileNotFoundException, ClassNotFoundException, IOException
	{

        //("1 --> Ordina Per Data di Acquisto");
        //("2 --> Ordina Per Costo Prodotto");



        Entry userLogged = Access.get_user_data(user);
        //System.out.println("---------- STORICO ----------");
        ArrayList<StoricoItem> storico = userLogged.getStorico();

        if  (choice.equals("1")){
            class CompareDataAcquisto implements Comparator<StoricoItem> {

                @Override
                public int compare(StoricoItem e1, StoricoItem e2) {

                    if (e1.getData_acquisto().getTimeInMillis()  < e2.getData_acquisto().getTimeInMillis())
                        return -1;
                    return 1;
                }
            }

            Collections.sort(storico,new CompareDataAcquisto());
            print_storico(storico, area_storico);

        } else {

            class ComparePrezzo implements Comparator<StoricoItem> {

                @Override
                public int compare(StoricoItem e1, StoricoItem e2) {

                    if (e1.getPrezzo()  < e2.getPrezzo())
                        return -1;
                    return 1;
                }
            }
            Collections.sort(storico,new ComparePrezzo());
            print_storico (storico, area_storico);
        }
	}

	/**
	 * Metodo che si occupa della visualizzazione dello storico dell'utente.
	 * 
	 * @param in ArrayList da scorrere.
	 * @param area_storico Oggetto JTextArea dove visualizzare i risultati.
	 */
    protected void print_storico (ArrayList<StoricoItem> in, JTextArea area_storico){

        for(StoricoItem record: in)
        {
            GregorianCalendar partenza = record.getData_acquisto();
            int giornoP = partenza.get(Calendar.DAY_OF_MONTH);
            int meseP = partenza.get(Calendar.MONTH);
            int annoP = partenza.get(Calendar.YEAR);
            area_storico.append("Deal: " + record.getDescription() + "| Data Acquisto: " + giornoP + "/" + (meseP + 1) + "/" + annoP + "| Prezzo: " + record.getPrezzo() + "\n");
        }
    }
	
    /**
	 *Questo metodo visualizza una vacanza, descrivendone i suoi dati.
	 * 
	 * @param dealVacanza Oggetto di tipo Vacanze che rappresenta l'offerta della Vacanza da visualizzare
	 */
    public void visualizzaVacanza(Vacanze dealVacanza, JTextArea show)
	{
		GregorianCalendar partenza = dealVacanza.getDataPartenzaViaggio();
		int giornoP = partenza.get(Calendar.DAY_OF_MONTH);
        int meseP = partenza.get(Calendar.MONTH);
        int annoP = partenza.get(Calendar.YEAR);
		GregorianCalendar scadenza = dealVacanza.getScadenzaOfferta();
		int giornoS = scadenza.get(Calendar.DAY_OF_MONTH);
        int meseS = scadenza.get(Calendar.MONTH);
        int annoS = scadenza.get(Calendar.YEAR);
		
		show.append("#Vacanza-"+dealVacanza.getIdViaggio()+"# Scadenza: "+giornoS+"/"+(meseS+1)+"/"+annoS+"| Localita: "+dealVacanza.getLocalitaViaggio()+"| Partenza: "+giornoP+"/"+(meseP+1)+"/"+annoP+"| Prezzo Pers. Singola: "+dealVacanza.getPrezzoPSingola()+"\n");
	}
   
    /**
     * Questo metodo visualizza un Bene di consumo, descrivendone i suoi dati.
     *
     * @param dealBene Oggetto di tipo beniDiConsumo che rappresenta l'offerta del bene da visualizzare
     */
	public void visualizzaBene(BeniDiConsumo dealBene, JTextArea show)
	{
        show.append("#Bene-" + dealBene.getIdBene() + "# Descrizione: " + dealBene.getDescrizioneBene() + " Prezzo: " + dealBene.getPrezzoBene() + "| Quantita in magazzino: " + dealBene.getBeniInStock() + "| Fornitore: " + dealBene.getFornitore() + "| Giudizio:  +" + dealBene.getGiudizioFornitore() + "\n");
	}

	
	/**
     *  Questo metodo visualizza una Prestazione d'opera, descrivendone i suoi dati.
     *
     * @param dealBene - oggetto di tipo beniDiConsumo che rappresenta l'offerta del bene da visualizzare
     */
    public void visualizzaPrestazione(PrestazioniDopera deal, JTextArea show)
    {
        show.append("#Prestazione-" + deal.getIdPrOpera() + "# Descrizione: " + deal.getDescrizione() + " Prezzo: " + deal.getPrezzoPrestazione() + "| Fornitore: " + deal.getFornitore() + "| Giudizio:  +" + deal.getGiudizioFornitore() + "\n");
    }

	/**
	 * Questo metodo visualizza una cena, descrivendone i suoi dati.
	 * 
	 * @param dealCena Oggetto di tipo CeneInRistoranti che rappresenta l'offerta della cena da visualizzare
	 */
	public void visualizzaCena(CeneInRistoranti dealCena, JTextArea show)
	{
		GregorianCalendar scadenzaCena = dealCena.getDataScadenzaOffertaCena();
		int giornoSC = scadenzaCena.get(Calendar.DAY_OF_MONTH);
        int meseSC = scadenzaCena.get(Calendar.MONTH);
        int annoSC = scadenzaCena.get(Calendar.YEAR);
        show.append("#Cena-" + dealCena.getIdCena() + "# Scadenza:" + giornoSC + "/" + (meseSC + 1) + "/" + annoSC + "| Ristorante: " + dealCena.getNomeRistorante() + "| Indirizzo: " + dealCena.getLuogo() + "| Descrizione: " + dealCena.getDescrizione_cena() + "| Costo: " + dealCena.getCostoPerPersona() + "| Disponibilita:" + dealCena.getNumCenedaVendere() + "cene" + "\n");
	}
	
	/**
	 * @return Restituisce il catalogo delle offerte
	 */
   
	public ArrayList<Utilizzabile> getCatalogo() {
        return catalogoOfferte;
    }

    /**
	 * Questo metodo restituisce tutti gli articoli con scadenza (vacanze, cene ristorante) che hanno scadenza tra una data iniziale e una data finale presi come parametri.
	 * 
	 * @param dataStart data iniziale
	 * @param dataEnd data finale
	 * @return un ArrayList contenente gli articoli compresi tra le due date inserite dall'utente
	 */
    public ArrayList<Utilizzabile> getDataExpire(GregorianCalendar dataStart, GregorianCalendar dataEnd)
    {
        ArrayList<Utilizzabile> arrayData = new ArrayList();

        for(Utilizzabile articolo: catalogoOfferte)
        {
            if(articolo instanceof Vacanze)
            {
                Vacanze dealVacanza = (Vacanze)articolo;
                if( ( dealVacanza.getScadenzaOfferta().before(dataEnd) ) && ( dealVacanza.getScadenzaOfferta().after(dataStart) ) )
                {
                    arrayData.add(dealVacanza);
                }
            }
            else if(articolo instanceof CeneInRistoranti)
            {
                CeneInRistoranti dealCena = (CeneInRistoranti)articolo;
                if( ( dealCena.getDataScadenzaOffertaCena().before(dataEnd) ) && ( dealCena.getDataScadenzaOffertaCena().after(dataStart) ) )
                {
                    arrayData.add(dealCena);
                }
            }
        }

        return arrayData;
    }

    /**
     * Questo metodo restituisce tutti gli articoli senza scadenza (beni, prestazioni) che sono offerti da fornitori con un giudizio superiore ad un valore preso come parametro.
     * 
     * @param votazione E' la votazione minima che deve avere un fornitore.
     * @return arrayDealNotExpire Ritorna un ArrayList contente tutti gli articoli senza scadenza
     */
    public  ArrayList<Utilizzabile> getDealNotExpire(int votazione)
    {
        ArrayList<Utilizzabile> arrayDealNotExpire = new ArrayList();

        for(Utilizzabile articolo: catalogoOfferte)
        {
            if(articolo instanceof BeniDiConsumo)
            {
                BeniDiConsumo dealBene = (BeniDiConsumo)articolo;
                if( dealBene.getGiudizioFornitore()>votazione )
                    arrayDealNotExpire.add(dealBene);
            }
            else if(articolo instanceof PrestazioniDopera)
            {
                PrestazioniDopera dealPrestazione = (PrestazioniDopera)articolo;
                if( dealPrestazione.getGiudizioFornitore()>votazione )
                    arrayDealNotExpire.add(dealPrestazione);
            }
        }
        return arrayDealNotExpire;
    }
 
    private ArrayList<Utilizzabile> catalogoOfferte;
}
