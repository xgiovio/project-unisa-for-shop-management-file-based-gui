package it.unisa.info13d.GestioneCatalogo;

import it.unisa.info13d.Articoli.Utilizzabile;
import it.unisa.info13d.Utility.ReShow;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with PC-STATION.
 * User: lebon
 * Date: 17/12/13
 * Time: 10.55
 */

/**
 * 
 * Classe si occupa di gestire l'interfaccia dell'amministratore
 *
 */
public class AdminSession {

	/**
	 * Questo metodo visualizza il menu per gli Amministratori
	 * @throws IOException 
	 * @throws ParseException 
	 * @throws ClassNotFoundException 
	 * @throws FileNotFoundException 
	 */
	public static void showAdminMenu(Catalogo catalogo, ReShow r, String username) throws FileNotFoundException, ClassNotFoundException, ParseException, IOException {
		System.out.println("------------ Admin Menu ------------");
		System.out.println("1 --> Nuovo Prodotto");            //Inserisce un nuovo prodotto in vendita nel catalogo
		System.out.println("2 --> Cancella Prodotto");         //Cancella un prodotto in offerta nel catalogo
		System.out.println("3 --> Offerte attive");            //Visualizza le offerte attive nel catalogo
		System.out.println("4 --> Offerte scadute");           //Visualizza le offerte non piu acquistabili
		System.out.println("5 --> Esci");           		 
		
		String sceltaMenu;
		System.out.print("\nOperazione: ");
		Scanner inputData = new Scanner(System.in);
		sceltaMenu = inputData.nextLine();
		//Controllo input. La scelta deve essere obbligatoriamente compresa tra 1 e 4
		for ( ;  !(sceltaMenu.equals("1"))&&!(sceltaMenu.equals("2"))&&!(sceltaMenu.equals("3"))&&!(sceltaMenu.equals("4"))&&!(sceltaMenu.equals("5")); ){
            System.out.println("Scelta Errata. Riprovare");
            System.out.print("Operazione: ");
            sceltaMenu = inputData.nextLine();
        }
		
		switch(sceltaMenu)
		{
			case "1":
				//catalogo.nuovoProdotto();
				break;
			case "2":
                //catalogo.cancellaProdotto();
				break;
			case "3":
                //catalogo.offerteAttive(username);
				break;
			case "4":
                //catalogo.offerteScadute(username);
				break;
			case "5":
               // r.reshow = false;
				break;
		}
		 
	}
}
