package it.unisa.info13d;

import it.unisa.info13d.GestioneCatalogo.Catalogo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 27/01/14
 * Time: 1.54
 */
public class Starter {

    /**
     *  Auto Populate the catalog if it's empty
     *
     * @param in_catalogo
     */

    public Starter (Catalogo in_catalogo){

        try{
            in_catalogo.nuovoProdottoBene("Orologio","152","20","Swatch","3");
            in_catalogo.nuovoProdottoBene("Maglia","25","45","Dolce e Gabbana","2");
            in_catalogo.nuovoProdottoBene("Tastiera","74","5","Logitech","5");
            in_catalogo.nuovoProdottoBene("TV LCD","700","32","Sony","1");
            in_catalogo.nuovoProdottoBene("Bandiera","14","2","USA Ent","4");

            in_catalogo.nuovoProdottoCena("Da Alfredo", "Avellino", "Ristorante moderno per ragazzi","24", "30/01/2014","25");
            in_catalogo.nuovoProdottoCena("Taverna del Gufo", "Avellino", "Ristorante moderno con ottimo vino","11", "23/03/2014","22");
            in_catalogo.nuovoProdottoCena("Mc Donald's", "Roma", "Fast Food con prodotti economici","5", "30/06/2014","600");


            in_catalogo.nuovoProdottoVacanza("Abruzzo", "15/02/2014", "02/02/2014", "150");
            in_catalogo.nuovoProdottoVacanza("Alpi", "16/04/2014", "12/03/2014", "400");
            in_catalogo.nuovoProdottoVacanza("Miami", "14/02/2014", "01/02/2014", "1500");

            in_catalogo.nuovoProdottoPrestazione("Venezia", "Dipinto in Strada", "Artisti Emergenti SRL", "5", "21");
            in_catalogo.nuovoProdottoPrestazione("Taormina", "Spettacolo Teatrale", "Fonz and Friends", "3", "10");



        }
        catch (FileNotFoundException err){}
        catch (IOException err){}
        catch (ClassNotFoundException err){}
        catch (ParseException err){}

    }

}
