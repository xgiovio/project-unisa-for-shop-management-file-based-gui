package it.unisa.info13d.Gui;

import it.unisa.info13d.Articoli.Global;
import it.unisa.info13d.Articoli.Utilizzabile;
import it.unisa.info13d.GestioneCatalogo.Catalogo;
import it.unisa.info13d.Login.Access;
import it.unisa.info13d.Login.LoginData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Arc2D;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 19/01/14
 * Time: 19.19
 */
public class AdminInterface extends JFrame{

	/**
	 * Genera la GUI per l'intereazione dell'admin con il sistema.
	 * 
	 * @param in_login_window
	 * @param in_load_catalogo
	 * @param in_login_data
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
    public AdminInterface(LoginWindow in_login_window, Catalogo in_load_catalogo, LoginData in_login_data) throws FileNotFoundException,IOException,ClassNotFoundException{

    login_window = in_login_window;
    load_catalogo= in_load_catalogo;
    login_data = in_login_data;

    //////////////////////////////////////////////////// begin main menu and balance

    JPanel mainWindow = new JPanel();
    mainWindow.setLayout(new BorderLayout());
    add(mainWindow);


    JPanel north = new JPanel();
    north.setLayout(new BorderLayout() );


    JMenuBar mainmenu = new JMenuBar();
    north.add(mainmenu,BorderLayout.NORTH);

   JMenu filemenu = new JMenu("File");
        JMenuItem logout_menu_item = new JMenuItem("Logout");
            class logout_action implements ActionListener   {


            public logout_action (AdminInterface in_location){
                location= in_location;
            }

            @Override
            public void actionPerformed(ActionEvent e) {


           location.setVisible(false);
                login_window.setVisible(true);


            }

            private AdminInterface location;
        }
            logout_menu_item.addActionListener(new logout_action(this));
        JMenuItem quit_menu_item = new JMenuItem("Quit");
            class exit_action implements ActionListener   {




            @Override
            public void actionPerformed(ActionEvent e) {
                         System.exit(0);

            }

        }
            quit_menu_item.addActionListener(new exit_action());

   JMenu searchmenu = new JMenu("Cerca");
        JMenuItem tutti_gli_articoli_attivi_id = new JMenuItem("Tutti gli articoli Attivi per ID");
            class tutti_gli_articoli_attivi_id_action implements ActionListener   {


            public tutti_gli_articoli_attivi_id_action (AdminInterface in_location, JTextArea in_offers,String in_choice){
                location= in_location;
                offers = in_offers;
                choice = in_choice;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    offers.setText("");
                load_catalogo.offerteAttive(login_data.getUsername(),offers,choice);

                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}

            }

            private AdminInterface location;
            private JTextArea offers;
            private String choice;
        }
            //action listner  added at offers declaration

        JMenuItem tutti_gli_articoli_attivi_data = new JMenuItem("Tutti gli articoli Attivi per Data");
        class tutti_gli_articoli_attivi_data_action implements ActionListener   {


            public tutti_gli_articoli_attivi_data_action (AdminInterface in_location, JTextArea in_offers,String in_choice){
                location= in_location;
                offers = in_offers;
                choice = in_choice;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    offers.setText("");
                    load_catalogo.offerteAttive(login_data.getUsername(),offers,choice);

                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}

            }

            private AdminInterface location;
            private JTextArea offers;
            private String choice;
        }
        //action listner  added at offers declaration

        JMenuItem tutti_gli_articoli_scaduti = new JMenuItem("Tutti gli articoli Scaduti");
        class tutti_gli_articoli_scaduti_action implements ActionListener   {


            public tutti_gli_articoli_scaduti_action (AdminInterface in_location, JTextArea in_offers){
                location= in_location;
                offers = in_offers;

            }

            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    offers.setText("");
                    load_catalogo.offerteScadute(login_data.getUsername(), offers);

                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}

            }

            private AdminInterface location;
            private JTextArea offers;

        }



   JMenu politicamenu = new JMenu("Politiche Sconto");

        JMenuItem visualizza_politiche = new JMenuItem("Visualizza Tutte le Politiche");
        class visualizza_politiche_action implements ActionListener   {


            public visualizza_politiche_action (AdminInterface in_location){
                location= in_location;

            }

            @Override
            public void actionPerformed(ActionEvent e) {


                    JPanel showInput = new JPanel();
                    showInput.setLayout(new GridLayout(0,1));
                    JLabel field1l = new JLabel("Per articoli maggiori di " + Global.getMin_num_articoli_per_sconto() +  " sconto del " + (Global.getSconto_per_min_num_articoli() * 100) + "%"   );
                    JLabel field2l = new JLabel("Per articoli nell'ultima settimana, sconto del " + (Global.getSconto_ultima_settimana() * 100) + "%");
                    JLabel field3l = new JLabel("Per cene nell'ultima settimana con piu del 50 % vendute, sconto ulteriore del " + (Global.getSconto_cene_sup_50() * 100) + "%" );

                    showInput.add(field1l);
                    showInput.add(field2l);
                    showInput.add(field3l);
                    JOptionPane.showMessageDialog(location, showInput);


            }

            private AdminInterface location;

        }
        visualizza_politiche.addActionListener(new visualizza_politiche_action(this));

        JMenuItem sconto_totale_articoli = new JMenuItem("Sconto sul Totale Articoli");
        class sconto_totale_articoli_action implements ActionListener   {


            public sconto_totale_articoli_action (AdminInterface in_location){
                location= in_location;

            }

            @Override
            public void actionPerformed(ActionEvent e) {


                     try{

                    JPanel showInput = new JPanel();
                    JLabel field1l = new JLabel("Numero Articoli");
                    JTextField field1 = new JTextField(10);
                    field1.setText("3");
                    JLabel field2l = new JLabel("Sconto 0-1");
                    JTextField field2 = new JTextField(10);
                    field2.setText("0.1");
                    showInput.add(field1l);
                    showInput.add(field1);
                    showInput.add(field2l);
                    showInput.add(field2);
                    JOptionPane.showMessageDialog(location, showInput);

                    String data = field1.getText();
                    int num_articoli = Integer.parseInt(data);


                    String data2 = field2.getText();
                    double sconto = Double.parseDouble(data2);

                         if (num_articoli >0 && sconto >=0 && sconto <= 1){


                    Global.set_sconto_min_articoli(num_articoli,sconto);
                         }else{
                             JOptionPane.showMessageDialog(location,
                                     "Input Non valido ",
                                     "Error",
                                     JOptionPane.ERROR_MESSAGE);

                         }

                     }
                     catch (NumberFormatException err){
                         JOptionPane.showMessageDialog(location,
                                 "Input Non valido ",
                                 "Error",
                                 JOptionPane.ERROR_MESSAGE);
                     }
                     catch (NullPointerException err){
                         JOptionPane.showMessageDialog(location,
                                 "Input Non valido ",
                                 "Error",
                                 JOptionPane.ERROR_MESSAGE);

                     }




            }

            private AdminInterface location;

        }
        sconto_totale_articoli.addActionListener(new sconto_totale_articoli_action(this));
        JMenuItem rimuovi_sconto_totale_articoli = new JMenuItem("Rimuovi Sconto sul Totale Articoli");
        class rimuovi_sconto_totale_articoli_action implements ActionListener   {


            public rimuovi_sconto_totale_articoli_action (AdminInterface in_location){
                location= in_location;

            }

            @Override
            public void actionPerformed(ActionEvent e) {

                Global.set_sconto_min_articoli(0,0);

                JOptionPane.showMessageDialog(location,
                        "Politica di sconto rimossa",
                        "Completata",
                        JOptionPane.INFORMATION_MESSAGE);

            }

            private AdminInterface location;

        }
        rimuovi_sconto_totale_articoli.addActionListener(new rimuovi_sconto_totale_articoli_action(this));


        JMenuItem sconto_articoli_ultima_settimana = new JMenuItem("Sconto su Articoli Ultima Settimana");
        class sconto_articoli_ultima_settimana_action implements ActionListener   {


            public sconto_articoli_ultima_settimana_action (AdminInterface in_location){
                location= in_location;

            }

            @Override
            public void actionPerformed(ActionEvent e) {


                try{

                JPanel showInput = new JPanel();
                JLabel field2l = new JLabel("Sconto Articoli Ultima Settimana 0-1");
                JTextField field2 = new JTextField(10);
                field2.setText("0.1");
                showInput.add(field2l);
                showInput.add(field2);
                JOptionPane.showMessageDialog(location, showInput);

                String data2 = field2.getText();
                double sconto = Double.parseDouble(data2);

                    if (sconto >= 0 && sconto <= 1){

                Global.setSconto_ultima_settimana(sconto);
                    }else{
                        JOptionPane.showMessageDialog(location,
                                "Input Non valido ",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }

                }
                catch (NumberFormatException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                catch (NullPointerException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }



            }

            private AdminInterface location;

        }
        sconto_articoli_ultima_settimana.addActionListener(new sconto_articoli_ultima_settimana_action(this));
        JMenuItem rimuovi_sconto_articoli_ultima_settimana = new JMenuItem("Rimuovi Sconto su Articoli Ultima Settimana");
        class rimuovi_sconto_articoli_ultima_settimana_action implements ActionListener   {


            public rimuovi_sconto_articoli_ultima_settimana_action (AdminInterface in_location){
                location= in_location;

            }

            @Override
            public void actionPerformed(ActionEvent e) {


                Global.setSconto_ultima_settimana(0);
                JOptionPane.showMessageDialog(location,
                        "Politica di sconto rimossa",
                        "Completata",
                        JOptionPane.INFORMATION_MESSAGE);

            }

            private AdminInterface location;

        }
        rimuovi_sconto_articoli_ultima_settimana.addActionListener(new rimuovi_sconto_articoli_ultima_settimana_action(this));

        JMenuItem sconto_cene_50_ultima_settimana = new JMenuItem("Sconto per >50% Cene Vendute Ultima Settimana");
        class sconto_cene_50_ultima_settimana_action implements ActionListener   {


            public sconto_cene_50_ultima_settimana_action (AdminInterface in_location){
                location= in_location;

            }

            @Override
            public void actionPerformed(ActionEvent e) {

                try{


                JPanel showInput = new JPanel();
                JLabel field2l = new JLabel("Sconto 0-1");
                JTextField field2 = new JTextField(10);
                field2.setText("0.1");
                showInput.add(field2l);
                showInput.add(field2);
                JOptionPane.showMessageDialog(location, showInput);

                String data2 = field2.getText();
                double sconto = Double.parseDouble(data2);

                    if (sconto >= 0 && sconto <= 1){

                Global.setSconto_cene_sup_50(sconto);
                    }else{
                        JOptionPane.showMessageDialog(location,
                                "Input Non valido ",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
                catch (NumberFormatException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                catch (NullPointerException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }



            }

            private AdminInterface location;

        }
        sconto_cene_50_ultima_settimana.addActionListener(new sconto_cene_50_ultima_settimana_action(this));
        JMenuItem rimuovi_sconto_cene_50_ultima_settimana = new JMenuItem("Rimuovi Sconto per >50% Cene Vendute Ultima Settimana");
        class rimuovi_sconto_cene_50_ultima_settimana_action implements ActionListener   {


            public rimuovi_sconto_cene_50_ultima_settimana_action (AdminInterface in_location){
                location= in_location;

            }

            @Override
            public void actionPerformed(ActionEvent e) {






                Global.setSconto_cene_sup_50(0);
                JOptionPane.showMessageDialog(location,
                        "Politica di sconto rimossa",
                        "Completata",
                        JOptionPane.INFORMATION_MESSAGE);




            }

            private AdminInterface location;

        }
        rimuovi_sconto_cene_50_ultima_settimana.addActionListener(new rimuovi_sconto_cene_50_ultima_settimana_action(this));




        mainmenu.add(filemenu);
        mainmenu.add(searchmenu);
        mainmenu.add(politicamenu);

        filemenu.add(logout_menu_item);
        filemenu.add(quit_menu_item);


        searchmenu.add(tutti_gli_articoli_attivi_id);
        searchmenu.add(tutti_gli_articoli_attivi_data);
        searchmenu.add(tutti_gli_articoli_scaduti);


        politicamenu.add(visualizza_politiche);
        politicamenu.add(sconto_totale_articoli);
        politicamenu.add(rimuovi_sconto_totale_articoli);
        politicamenu.add(sconto_articoli_ultima_settimana);
        politicamenu.add(rimuovi_sconto_articoli_ultima_settimana);
        politicamenu.add(sconto_cene_50_ultima_settimana);
        politicamenu.add(rimuovi_sconto_cene_50_ultima_settimana);




    JPanel AggiungiProdottiPanel = new JPanel();
        JLabel add_product_label = new JLabel("Aggiungi un nuovo Prodotto" );
        add_product_label.setPreferredSize(new Dimension(180,60));
        JButton aggiungi_bene = new JButton("Bene");
        class aggiungi_bene_action implements ActionListener   {


            public aggiungi_bene_action (AdminInterface in_location, JTextArea in_offers){
                location= in_location;
                offers = in_offers;

            }

            @Override
            public void actionPerformed(ActionEvent e) {

                try{


                JPanel showInput = new JPanel();
                showInput.setLayout(new GridLayout(0,2));

                JLabel descrizionel = new JLabel("Descrizione");
                JTextField descrizione = new JTextField(15);

                JLabel prezzol = new JLabel("Prezzo");
                JTextField prezzo = new JTextField(15);

                JLabel quantital = new JLabel("Quantita");
                JTextField quantita = new JTextField(15);

                JLabel fornitorel = new JLabel("Fornitore");
                JTextField fornitore = new JTextField(15);

                JLabel giudiziol = new JLabel("Giudizio");
                JTextField giudizio = new JTextField(15);


                showInput.add(descrizionel);
                showInput.add(descrizione);
                showInput.add(prezzol);
                showInput.add(prezzo);
                showInput.add(quantital);
                showInput.add(quantita);
                showInput.add(fornitorel);
                showInput.add(fornitore);
                showInput.add(giudiziol);
                showInput.add(giudizio);


                JOptionPane.showMessageDialog(location, showInput);


                    double checkd = Double.parseDouble(prezzo.getText());
                    int checki = Integer.parseInt(quantita.getText());
                    checki = Integer.parseInt(giudizio.getText());

                    if (Double.parseDouble(prezzo.getText()) >= 0 && Integer.parseInt(quantita.getText()) > 0 && Integer.parseInt(giudizio.getText())>=1 && Integer.parseInt(giudizio.getText())<=5  ){

                load_catalogo.nuovoProdottoBene(descrizione.getText(),prezzo.getText(),quantita.getText(),fornitore.getText(),giudizio.getText());
                    offers.setText("");
                    load_catalogo.offerteAttive(login_data.getUsername(),offers,"0");
                    }
                    else{
                        JOptionPane.showMessageDialog(location,
                                "Input Non valido ",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);

                    }

                }

                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}
                catch (ParseException err){}
                catch (NumberFormatException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                catch (NullPointerException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }


            }

            private AdminInterface location;
            private JTextArea offers;

        }


        JButton aggiungi_cena = new JButton("Cena");
        class aggiungi_cena_action implements ActionListener   {


            public aggiungi_cena_action (AdminInterface in_location, JTextArea in_offers){
                location= in_location;
                offers = in_offers;


            }

            @Override
            public void actionPerformed(ActionEvent e) {

                try{


                    JPanel showInput = new JPanel();
                    showInput.setLayout(new GridLayout(0,2));

                    JLabel ristorantel = new JLabel("Ristorante");
                    JTextField ristorante = new JTextField(15);

                    JLabel localital = new JLabel("Localita");
                    JTextField localita = new JTextField(15);

                    JLabel descrizionel = new JLabel("Descrizione");
                    JTextField descrizione = new JTextField(15);

                    JLabel prezzol = new JLabel("Prezzo");
                    JTextField prezzo = new JTextField(15);

                    JLabel scadenzal = new JLabel("Scadenza");
                    JTextField scadenza = new JTextField(15);
                    scadenza.setText("GG/MM/AAAA");

                    JLabel cenedavendl = new JLabel("Cene Da Vendere");
                    JTextField cenedavend = new JTextField(15);





                    showInput.add(ristorantel);
                    showInput.add(ristorante);
                    showInput.add(localital);
                    showInput.add(localita);
                    showInput.add(descrizionel);
                    showInput.add(descrizione);
                    showInput.add(prezzol);
                    showInput.add(prezzo);
                    showInput.add(scadenzal);
                    showInput.add(scadenza);
                    showInput.add(cenedavendl);
                    showInput.add(cenedavend);


                    JOptionPane.showMessageDialog(location, showInput);

                    double checkd = Double.parseDouble(prezzo.getText());
                    int checki = Integer.parseInt(cenedavend.getText());

                    if ( Double.parseDouble(prezzo.getText()) >= 0 && Integer.parseInt(cenedavend.getText())>0  ) {


                    load_catalogo.nuovoProdottoCena(ristorante.getText(), localita.getText(), descrizione.getText(), prezzo.getText(), scadenza.getText(), cenedavend.getText());
                    offers.setText("");
                    load_catalogo.offerteAttive(login_data.getUsername(),offers,"0");
                    }else{
                        JOptionPane.showMessageDialog(location,
                                "Input Non valido ",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);

                    }
                }

                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}
                catch (ParseException err){}
                catch (NumberFormatException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                catch (NullPointerException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }


            }

            private AdminInterface location;
            private JTextArea offers;

        }

        JButton aggiungi_vacanza = new JButton("Viaggio");
        class aggiungi_vacanza_action implements ActionListener   {


            public aggiungi_vacanza_action (AdminInterface in_location, JTextArea in_offers){
                location= in_location;
                offers = in_offers;

            }

            @Override
            public void actionPerformed(ActionEvent e) {

                try{


                    JPanel showInput = new JPanel();
                    showInput.setLayout(new GridLayout(0,2));

                    JLabel localital = new JLabel("Localita");
                    JTextField localita = new JTextField(15);

                    JLabel datastartl = new JLabel("Data Partenza");
                    JTextField datastart = new JTextField(15);

                    JLabel dataendl = new JLabel("Data Fine Offerta");
                    JTextField dataend = new JTextField(15);

                    JLabel prezzol = new JLabel("Prezzo");
                    JTextField prezzo = new JTextField(15);





                    showInput.add(localital);
                    showInput.add(localita);
                    showInput.add(datastartl);
                    showInput.add(datastart);
                    showInput.add(dataendl);
                    showInput.add(dataend);
                    showInput.add(prezzol);
                    showInput.add(prezzo);


                    JOptionPane.showMessageDialog(location, showInput);

                    if (Double.parseDouble(prezzo.getText()) >= 0){


                    load_catalogo.nuovoProdottoVacanza(localita.getText(), datastart.getText(), dataend.getText(), prezzo.getText());
                    offers.setText("");
                    load_catalogo.offerteAttive(login_data.getUsername(),offers,"0");
                    }
                    else{
                        JOptionPane.showMessageDialog(location,
                                "Input Non valido ",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);


                    }

                }

                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}
                catch (ParseException err){}
                catch (NumberFormatException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                catch (NullPointerException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }


            }

            private AdminInterface location;
            private JTextArea offers;

        }

        JButton aggiungi_prestazione = new JButton("Prestazione");
        class aggiungi_prestazione_action implements ActionListener   {


            public aggiungi_prestazione_action (AdminInterface in_location,JTextArea in_offers){
                location= in_location;
                offers = in_offers;

            }

            @Override
            public void actionPerformed(ActionEvent e) {

                try{


                    JPanel showInput = new JPanel();
                    showInput.setLayout(new GridLayout(0,2));

                    JLabel localital = new JLabel("Localita");
                    JTextField localita = new JTextField(15);

                    JLabel descrizionel = new JLabel("Descrizione");
                    JTextField descrizione = new JTextField(15);

                    JLabel fornitorel = new JLabel("Fornitore");
                    JTextField fornitore = new JTextField(15);

                    JLabel giudiziol = new JLabel("Giudizio");
                    JTextField giudizio = new JTextField(15);

                    JLabel prezzol = new JLabel("Prezzo");
                    JTextField prezzo = new JTextField(15);





                    showInput.add(localital);
                    showInput.add(localita);
                    showInput.add(descrizionel);
                    showInput.add(descrizione);
                    showInput.add(fornitorel);
                    showInput.add(fornitore);
                    showInput.add(giudiziol);
                    showInput.add(giudizio);
                    showInput.add(prezzol);
                    showInput.add(prezzo);


                    JOptionPane.showMessageDialog(location, showInput);

                    if (Integer.parseInt(giudizio.getText()) >= 1 &&  Integer.parseInt(giudizio.getText()) <= 5 & Double.parseDouble(prezzo.getText())>= 0 ){
                    load_catalogo.nuovoProdottoPrestazione(localita.getText(), descrizione.getText(), fornitore.getText(), giudizio.getText(), prezzo.getText());
                    offers.setText("");
                    load_catalogo.offerteAttive(login_data.getUsername(),offers,"0");
                    }else{
                        JOptionPane.showMessageDialog(location,
                                "Input Non valido ",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }

                }

                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}
                catch (ParseException err){}
                catch (NumberFormatException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                catch (NullPointerException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }


            }

            private AdminInterface location;
            private JTextArea offers;

        }




        AggiungiProdottiPanel.add(add_product_label);
        AggiungiProdottiPanel.add(aggiungi_bene);
        AggiungiProdottiPanel.add(aggiungi_cena);
        AggiungiProdottiPanel.add(aggiungi_vacanza);
        AggiungiProdottiPanel.add(aggiungi_prestazione);








    north.add(AggiungiProdottiPanel,BorderLayout.CENTER);

     mainWindow.add(north,BorderLayout.NORTH);




        //////////////////////////////////////////////////// begin center area for all offers


        JTextArea offers = new JTextArea(500,200);
        offers.setEditable(false);
        JScrollPane scrollarea = new JScrollPane(offers);
        offers.setText("Utilizza il menu Cerca per popolare questa lista");
        mainWindow.add(scrollarea,BorderLayout.CENTER);
        tutti_gli_articoli_attivi_id.addActionListener(new tutti_gli_articoli_attivi_id_action(this,offers,"0"));
        tutti_gli_articoli_attivi_data.addActionListener(new tutti_gli_articoli_attivi_data_action(this,offers,"1"));
        tutti_gli_articoli_scaduti.addActionListener(new tutti_gli_articoli_scaduti_action(this,offers));


        aggiungi_bene.addActionListener(new aggiungi_bene_action(this,offers));
        aggiungi_cena.addActionListener(new aggiungi_cena_action(this,offers));
        aggiungi_vacanza.addActionListener(new aggiungi_vacanza_action(this,offers));
        aggiungi_prestazione.addActionListener(new aggiungi_prestazione_action(this,offers));




        JPanel acquista_panel = new JPanel();

        JLabel idlabel = new JLabel("ID");
        JTextField id = new JTextField(5);
        JButton rimuovi_prodotto = new JButton("Rimuovi Prodotto");


        /////old class to replace with rimuovi prodotto
        class rimuovi_prodotto_action implements ActionListener   {


            public rimuovi_prodotto_action (AdminInterface in_location, JTextField in_id, JTextArea in_offers ){
                id = in_id;
                location= in_location;
                offers = in_offers;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                try{


                        if (Integer.parseInt(id.getText()) >= 0 ){
                            boolean found = load_catalogo.cancellaProdotto(id.getText());
                             if (found ){
                                 JOptionPane.showMessageDialog(location,
                                         "Prodotto Rimosso ",
                                         "Completato",
                                         JOptionPane.INFORMATION_MESSAGE);
                                 offers.setText("");
                                 load_catalogo.offerteAttive(login_data.getUsername(),offers,"0");

                             }else{
                                 JOptionPane.showMessageDialog(location,
                                         "Impossibile rimuovere il prodotto indicato ",
                                         "Error",
                                         JOptionPane.ERROR_MESSAGE);
                             }


                        }else {
                            JOptionPane.showMessageDialog(location,
                                    "Input Non valido ",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);

                        }


                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}
                catch (NumberFormatException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                catch (NullPointerException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }




            }

            private JTextField id;
            private AdminInterface location;
            private JTextArea offers;

        }
        rimuovi_prodotto.addActionListener(new rimuovi_prodotto_action(this,id,offers));



        acquista_panel.add(idlabel);
        acquista_panel.add(id);
        acquista_panel.add(rimuovi_prodotto);



        mainWindow.add(acquista_panel,BorderLayout.SOUTH);




        setSize(650, 350);
        setLocation(login_window.getLocation());
        setTitle("Admin Logged");
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }


    ArrayList<Utilizzabile> Carrello = new ArrayList<Utilizzabile>();
    LoginWindow login_window;
    Catalogo load_catalogo;
    LoginData login_data;


}
