package it.unisa.info13d.Gui;


import it.unisa.info13d.Articoli.Global;
import it.unisa.info13d.Articoli.Utilizzabile;
import it.unisa.info13d.GestioneCatalogo.Catalogo;
import it.unisa.info13d.Login.Access;
import it.unisa.info13d.Login.LoginData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.concurrent.Callable;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 19/01/14
 * Time: 19.19
 */
public class UserInterface extends JFrame{

	/**
	 * Genera la GUI per l'interazione dell'utente con il sistema.
	 * 
	 * @param in_login_window
	 * @param in_load_catalogo
	 * @param in_login_data
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
    public UserInterface( LoginWindow in_login_window ,Catalogo in_load_catalogo, LoginData in_login_data) throws FileNotFoundException,IOException,ClassNotFoundException{

    login_window = in_login_window;
    load_catalogo= in_load_catalogo;
    login_data = in_login_data;

    //////////////////////////////////////////////////// begin main menu and balance

    JPanel mainWindow = new JPanel();
    mainWindow.setLayout(new BorderLayout());
    add(mainWindow);


    JPanel north = new JPanel();
    north.setLayout(new BorderLayout() );


    JMenuBar mainmenu = new JMenuBar();
    north.add(mainmenu,BorderLayout.NORTH);

   JMenu filemenu = new JMenu("File");
        JMenuItem logout_menu_item = new JMenuItem("Logout");
            class logout_action implements ActionListener   {


            public logout_action (UserInterface in_location){
                location= in_location;
            }

            @Override
            public void actionPerformed(ActionEvent e) {


           location.setVisible(false);
                login_window.setVisible(true);


            }

            private UserInterface location;
        }
            logout_menu_item.addActionListener(new logout_action(this));
        JMenuItem quit_menu_item = new JMenuItem("Quit");
            class exit_action implements ActionListener   {




            @Override
            public void actionPerformed(ActionEvent e) {
                         System.exit(0);

            }

        }
            quit_menu_item.addActionListener(new exit_action());

   JMenu searchmenu = new JMenu("Cerca");
        JMenuItem tutti_gli_articoli_attivi = new JMenuItem("Tutti gli articoli Attivi");
            class tutti_gli_articoli_attivi_action implements ActionListener   {


            public tutti_gli_articoli_attivi_action (UserInterface in_location, JTextArea in_offers){
                location= in_location;
                offers = in_offers;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    offers.setText("");
                load_catalogo.offerteAttive(login_data.getUsername(),offers,"0");

                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}

            }

            private UserInterface location;
            private JTextArea offers;
        }
            //action listner  added at offers declaration

        JMenuItem articoli_con_scadenza_menu_item = new JMenuItem("Articoli con Scadenza");
        class tutti_gli_articoli_con_scadenza_action implements ActionListener   {


            public tutti_gli_articoli_con_scadenza_action (UserInterface in_location, JTextArea in_offers){
                location= in_location;
                offers = in_offers;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    offers.setText("");


                    JPanel showInput = new JPanel();
                    JLabel field1l = new JLabel("Data Start");
                    JTextField field1 = new JTextField(10);
                    field1.setText("GG/MM/AAAA");
                    JLabel field2l = new JLabel("Data Fine");
                    JTextField field2 = new JTextField(10);
                    field2.setText("GG/MM/AAAA");
                    showInput.add(field1l);
                    showInput.add(field1);
                    showInput.add(field2l);
                    showInput.add(field2);
                    JOptionPane.showMessageDialog(location, showInput);

                    String marcatore="/";
                    String data = field1.getText();
                    String temp[];
                    temp = data.split(marcatore);
                    GregorianCalendar dataPartenza = new GregorianCalendar((Integer.parseInt(temp[2])),(Integer.parseInt(temp[1]))-1,(Integer.parseInt(temp[0])));

                    String data2 = field2.getText();
                    String temp2[];
                    temp2 = data2.split(marcatore);
                    GregorianCalendar dataScad = new GregorianCalendar((Integer.parseInt(temp2[2])),(Integer.parseInt(temp2[1]))-1,(Integer.parseInt(temp2[0])));



                    load_catalogo.offerteAttiveConScadenza(login_data.getUsername(), offers, "0",dataPartenza,dataScad);

                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}
                catch (NumberFormatException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                catch (NullPointerException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }

            }

            private UserInterface location;
            private JTextArea offers;
        }
        //action listner  added at offers declaration

        JMenuItem articoli_senza_scadenza_menu_item = new JMenuItem("Articoli senza Scadenza");
        class tutti_gli_articoli_senza_scadenza_action implements ActionListener   {


            public tutti_gli_articoli_senza_scadenza_action (UserInterface in_location, JTextArea in_offers){
                location= in_location;
                offers = in_offers;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    offers.setText("");


                    int valutazione = Integer.parseInt(
                            JOptionPane.showInputDialog(
                                    location, "Inserire valutazione fornitore","Valutazione"));

                    if (valutazione <= 5 && valutazione >=1){
                    load_catalogo.offerteAttiveSenzaScadenza(login_data.getUsername(), offers, "0", valutazione);
                    }else{
                        JOptionPane.showMessageDialog(location,
                                "Input Non valido ",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }

                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}
                catch (NumberFormatException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                catch (NullPointerException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }

            }

            private UserInterface location;
            private JTextArea offers;
        }
        //action listner  added at offers declaration
   JMenu visualizzamenu = new JMenu("Visualizza");
        JMenuItem storico_data_menu_item = new JMenuItem("Storico Utente per Data");
        class storico_data_menu_item_action implements ActionListener   {


            public storico_data_menu_item_action (UserInterface in_location){
                location= in_location;

            }

            @Override
            public void actionPerformed(ActionEvent e) {
                try{



                    JPanel show_storico = new JPanel();
                    JTextArea area_storico = new JTextArea(10,45);
                    JScrollPane scrollarea = new JScrollPane(area_storico);


                    show_storico.add(scrollarea);
                    load_catalogo.visualizzaStorico(login_data.getUsername(),"1",area_storico);

                    JOptionPane.showMessageDialog(location, show_storico);





                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}

            }

            private UserInterface location;

        }
        storico_data_menu_item.addActionListener(new storico_data_menu_item_action(this));
        JMenuItem storico_costo_menu_item = new JMenuItem("Storico Utente per Costo");
        class storico_costo_menu_item_action implements ActionListener   {


            public storico_costo_menu_item_action (UserInterface in_location){
                location= in_location;

            }

            @Override
            public void actionPerformed(ActionEvent e) {
                try{



                    JPanel show_storico = new JPanel();
                    JTextArea area_storico = new JTextArea(10,45);
                    JScrollPane scrollarea = new JScrollPane(area_storico);


                    show_storico.add(scrollarea);
                    load_catalogo.visualizzaStorico(login_data.getUsername(),"2",area_storico);

                    JOptionPane.showMessageDialog(location, show_storico);





                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}

            }

            private UserInterface location;

        }
        storico_costo_menu_item.addActionListener(new storico_costo_menu_item_action(this));

        JMenu politichemenu = new JMenu("Politiche");
        JMenuItem visualizza_politiche = new JMenuItem("Visualizza Tutte le Politiche");
        class visualizza_politiche_action implements ActionListener   {


            public visualizza_politiche_action (UserInterface in_location){
                location= in_location;

            }

            @Override
            public void actionPerformed(ActionEvent e) {


                JPanel showInput = new JPanel();
                showInput.setLayout(new GridLayout(0,1));
                JLabel field1l = new JLabel("Per articoli maggiori di " + Global.getMin_num_articoli_per_sconto() +  " sconto del " + (Global.getSconto_per_min_num_articoli() * 100) + "%"   );
                JLabel field2l = new JLabel("Per articoli nell'ultima settimana, sconto del " + (Global.getSconto_ultima_settimana() * 100) + "%");
                JLabel field3l = new JLabel("Per cene nell'ultima settimana con piu del 50 % vendute, sconto ulteriore del " + (Global.getSconto_cene_sup_50() * 100) + "%" );

                showInput.add(field1l);
                showInput.add(field2l);
                showInput.add(field3l);
                JOptionPane.showMessageDialog(location, showInput);


            }

            private UserInterface location;

        }
        visualizza_politiche.addActionListener(new visualizza_politiche_action(this));

        mainmenu.add(filemenu);
        mainmenu.add(searchmenu);
        mainmenu.add(visualizzamenu);
        mainmenu.add(politichemenu);

        filemenu.add(logout_menu_item);
        filemenu.add(quit_menu_item);

        searchmenu.add(tutti_gli_articoli_attivi);
        searchmenu.add(articoli_con_scadenza_menu_item);
        searchmenu.add(articoli_senza_scadenza_menu_item);


        visualizzamenu.add(storico_data_menu_item);
        visualizzamenu.add(storico_costo_menu_item);

        politichemenu.add(visualizza_politiche);




    JPanel BilancioPanel = new JPanel();
        JLabel bilancio = new JLabel("Bilancio" + " " + login_data.getUsername() + " = " + (Access.get_user_data(login_data.getUsername())).getBalance()  );
        bilancio.setPreferredSize(new Dimension(150,60));
        JButton acquista_credito = new JButton("Acquista Credito");
        BilancioPanel.add(bilancio);
        BilancioPanel.add(acquista_credito);

        class acquista_action implements ActionListener   {


            public acquista_action (UserInterface in_location, JLabel in_l){
                l = in_l;
                location= in_location;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                 try{

                    String importo_da_ricaricare= JOptionPane.showInputDialog(location,"Inserire l'importo da ricaricare ");
                    if (Double.parseDouble(importo_da_ricaricare) > 0){

                        load_catalogo.aggiungiCredito(login_data.getUsername(), importo_da_ricaricare  );
                        l.setText("Bilancio" + " " + login_data.getUsername() + " = " + (Access.get_user_data(login_data.getUsername())).getBalance());

                    } else{
                        JOptionPane.showMessageDialog(location,
                                "Input Non valido ",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                 }
                 catch (FileNotFoundException err){}
                 catch (IOException err){}
                 catch (ClassNotFoundException err){}
                 catch (NumberFormatException err){
                     JOptionPane.showMessageDialog(location,
                             "Input Non valido ",
                             "Error",
                             JOptionPane.ERROR_MESSAGE);
                 }
                 catch (NullPointerException err){
                     JOptionPane.showMessageDialog(location,
                             "Input Non valido ",
                             "Error",
                             JOptionPane.ERROR_MESSAGE);

                 }




            }

            private JLabel l;
            private UserInterface location;
        }
        acquista_credito.addActionListener(new acquista_action(this,bilancio));





    north.add(BilancioPanel,BorderLayout.CENTER);

     mainWindow.add(north,BorderLayout.NORTH);




        //////////////////////////////////////////////////// begin center area for all offers


        JTextArea offers = new JTextArea(500,200);
        offers.setEditable(false);
        JScrollPane scrollarea = new JScrollPane(offers);
        offers.setText("Utilizza il menu Cerca per popolare questa lista");
        mainWindow.add(scrollarea,BorderLayout.CENTER);
        tutti_gli_articoli_attivi.addActionListener(new tutti_gli_articoli_attivi_action(this,offers));
        articoli_con_scadenza_menu_item.addActionListener(new tutti_gli_articoli_con_scadenza_action(this,offers));
        articoli_senza_scadenza_menu_item.addActionListener(new tutti_gli_articoli_senza_scadenza_action(this,offers));



        JPanel acquista_panel = new JPanel();

        JLabel idlabel = new JLabel("ID");
        JTextField id = new JTextField(5);
        JButton aggiungi_a_carrello = new JButton("Aggiungi al Carrello");
        JLabel prodotti_nel_carello = new JLabel("            Carrello: " + Carrello.size() + " Prodotti");
        JButton acquista = new JButton("Completa Acquisto");

        class aggiungi_al_carrello_action implements ActionListener   {


            public aggiungi_al_carrello_action (UserInterface in_location, JTextField in_id, JTextArea in_offers, JLabel in_count){
                id = in_id;
                location= in_location;
                offers = in_offers;
                count = in_count;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                try{

                    if (offers.getText().isEmpty()){
                        JOptionPane.showMessageDialog(location,
                                "Dovresti prima aggiornare l'elenco delle offerte attive ",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    } else{

                        if (Integer.parseInt(id.getText()) >= 0 ){
                            int found = load_catalogo.search_product_id(login_data.getUsername(),id.getText());
                             if (found > -1){
                                Carrello.add(load_catalogo.getCatalogo().get(found));
                                 count.setText("            Corrello: " + Carrello.size() + " Prodotti");

                             }else{
                                 JOptionPane.showMessageDialog(location,
                                         "Impossibile aggiungere il prodotto indicato ",
                                         "Error",
                                         JOptionPane.ERROR_MESSAGE);
                             }


                        }else {
                            JOptionPane.showMessageDialog(location,
                                    "Input Non valido ",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);

                        }

                    }
                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}
                catch (NumberFormatException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                catch (NullPointerException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }




            }

            private JTextField id;
            private UserInterface location;
            private JTextArea offers;
            private JLabel count;
        }
        aggiungi_a_carrello.addActionListener(new aggiungi_al_carrello_action(this,id,offers,prodotti_nel_carello));

        class completa_acquisto implements ActionListener   {


            public completa_acquisto (UserInterface in_location, JLabel in_count, JLabel in_bilancio){
                count = in_count;
                location= in_location;
                bilancio = in_bilancio;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                try{

                    if (Carrello.size() == 0){
                        JOptionPane.showMessageDialog(location,
                                "Carrello Vuoto ",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    } else{

                        boolean status;
                        status = load_catalogo.acquistaProdotti(login_data.getUsername(),Carrello);

                        if (status){


                            count.setText("            Carrello: 0 Prodotti");
                            bilancio.setText("Bilancio" + " " + login_data.getUsername() + " = " + (Access.get_user_data(login_data.getUsername())).getBalance()  );

                            JOptionPane.showMessageDialog(location,
                                    "Acquisto Completato",
                                    "Completato",
                                    JOptionPane.INFORMATION_MESSAGE);

                        }else{
                            JOptionPane.showMessageDialog(location,
                                    "Impossibile completare Acquisto ",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }

                    }
                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}
                catch (NumberFormatException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                catch (NullPointerException err){
                    JOptionPane.showMessageDialog(location,
                            "Input Non valido ",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }




            }

            private JLabel count;
            private UserInterface location;
            private JLabel bilancio;
        }
        acquista.addActionListener(new completa_acquisto(this,prodotti_nel_carello,bilancio));

        acquista_panel.add(idlabel);
        acquista_panel.add(id);
        acquista_panel.add(aggiungi_a_carrello);
        acquista_panel.add(prodotti_nel_carello);
        acquista_panel.add(acquista);


        mainWindow.add(acquista_panel,BorderLayout.SOUTH);




        setSize(650, 350);
        setLocation(login_window.getLocation());
        setTitle("User " + Access.get_user_data(login_data.getUsername()).getUser() + " Logged");
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }


    ArrayList<Utilizzabile> Carrello = new ArrayList<Utilizzabile>();
    LoginWindow login_window;
    Catalogo load_catalogo;
    LoginData login_data;


}
