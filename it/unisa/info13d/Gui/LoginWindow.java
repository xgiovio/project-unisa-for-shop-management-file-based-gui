package it.unisa.info13d.Gui;

import it.unisa.info13d.Login.Access;
import it.unisa.info13d.Login.LoginData;
import it.unisa.info13d.GestioneCatalogo.*;
import it.unisa.info13d.Starter;
import it.unisa.info13d.Utility.IdCounter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 19/01/14
 * Time: 16.25
 */

/**
 * 
 * Questa classe di occupa di gestire la GUI per l'utente.
 *
 */
public class LoginWindow extends JFrame  {

    public LoginWindow ()  {

        JPanel mainWindow = new JPanel();
        mainWindow.setLayout(new BorderLayout());
        add(mainWindow);

        JPanel center_panel = new JPanel();
        center_panel.setLayout(new BoxLayout(center_panel, BoxLayout.PAGE_AXIS) );
        mainWindow.add(center_panel,BorderLayout.CENTER);




        JPanel user_panel = new JPanel();
        center_panel.add(user_panel);

        JPanel pass_panel = new JPanel();
        center_panel.add(pass_panel);


        JLabel userl = new JLabel("User          ");
        JTextField usert= new JTextField(10);

        JLabel passl = new JLabel("Password");
        JPasswordField passt= new JPasswordField(10);


        user_panel.add(userl);
        user_panel.add(usert);
        pass_panel.add(passl);
        pass_panel.add(passt);



        JPanel buttons_panel = new JPanel();


        JButton submit_login = new JButton("Login");
        JButton submit_register = new JButton("Go to Register Form");

        center_panel.add(buttons_panel);

        buttons_panel.add(submit_login);
        buttons_panel.add(submit_register);


        class login_action implements ActionListener {


            public login_action (LoginWindow in_actual,JTextField in_name, JPasswordField in_password, JTextField in_location, boolean in_action){
                name = in_name;
                password = in_password;
                location = in_location;
                action = in_action;
                actual = in_actual;


            }


            @Override
            public void actionPerformed(ActionEvent e)  {
                try{
                    if (Access.search_username(name.getText(),String.valueOf(password.getPassword()))){
                        LoginData login = new LoginData(name.getText(),String.valueOf(password.getPassword()),location.getText(),action);
                        IdCounter.restore_counter();
                        Catalogo load_catalogo = new Catalogo();
                        if (login.getType().equals("Admin")){
                              actual.setVisible(false);
                              AdminInterface a = new AdminInterface(actual,load_catalogo,login);
                        }else{
                            actual.setVisible(false);
                            UserInterface c = new UserInterface(actual,load_catalogo,login);
                        }


                    } else{

                        JOptionPane.showMessageDialog(actual,
                                "User e/o Pass Inesistenti ",
                                "Login Error",
                                JOptionPane.ERROR_MESSAGE);
                    }

                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}


            }

            private JTextField name;
            private JPasswordField password;
            private JTextField location;
            private boolean action;
            private LoginWindow actual;


        }
        class register_action implements ActionListener {


            public register_action (LoginWindow in_w){
                w = in_w;


            }


            @Override
            public void actionPerformed(ActionEvent e)  {
                w.setVisible(false);
                RegisterWindow r = new RegisterWindow(w,w.getLocation());
            }

            LoginWindow w;


        }
        submit_login.addActionListener(new login_action(this,usert,passt,usert,false));
        submit_register.addActionListener(new register_action(this));



       setSize(250, 130);
       setLocation(150, 150);
       setTitle("Exam APP");
       setVisible(true);
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


    }





}
