package it.unisa.info13d.Gui;

import it.unisa.info13d.Login.Access;
import it.unisa.info13d.Login.LoginData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 19/01/14
 * Time: 16.25
 */
public class RegisterWindow extends JFrame  {

	/**
	 * Genera la GUI per la procedura di registrazione dell'utente al sistema.
	 * 
	 * @param in_back
	 * @param in_point
	 */
    public RegisterWindow( LoginWindow in_back, Point in_point )  {

        setLocation(in_point);
        back = in_back;

        JPanel mainWindow = new JPanel();
        mainWindow.setLayout(new BorderLayout());
        add(mainWindow);

        JPanel center_panel = new JPanel();
        center_panel.setLayout(new BoxLayout(center_panel, BoxLayout.PAGE_AXIS) );
        mainWindow.add(center_panel,BorderLayout.CENTER);




        JPanel user_panel = new JPanel();
        center_panel.add(user_panel);

        JPanel pass_panel = new JPanel();
        center_panel.add(pass_panel);

        JPanel location_panel = new JPanel();
        center_panel.add(location_panel);



        JLabel userl = new JLabel("User          ");
        JTextField usert= new JTextField(10);

        JLabel passl = new JLabel("Password");
        JPasswordField passt= new JPasswordField(10);

        JLabel locationl = new JLabel("Location   ");
        JTextField locationt= new JTextField(10);


        user_panel.add(userl);
        user_panel.add(usert);
        pass_panel.add(passl);
        pass_panel.add(passt);
        location_panel.add(locationl);
        location_panel.add(locationt);



        JPanel buttons_panel = new JPanel();


        JButton submit_register = new JButton("Register");
        JButton submit_back = new JButton("Back");

        center_panel.add(buttons_panel);

        buttons_panel.add(submit_register);
        buttons_panel.add(submit_back);


        class register_action implements ActionListener {


            public register_action (RegisterWindow in_actual,JTextField in_name, JPasswordField in_password, JTextField in_location, boolean in_action){
                name = in_name;
                password = in_password;
                location = in_location;
                action = in_action;
                actual = in_actual;


            }


            @Override
            public void actionPerformed(ActionEvent e)  {
                try{
                    if ( location.getText().isEmpty() || name.getText().isEmpty() || String.valueOf(password.getPassword()).isEmpty() ){
                        JOptionPane.showMessageDialog(actual,
                                "Completare tutti i campi ",
                                "Register Error",
                                JOptionPane.ERROR_MESSAGE);
                    } else{

                        LoginData login = new LoginData(name.getText(),String.valueOf(password.getPassword()),location.getText(),action);
                        JOptionPane.showMessageDialog(actual,
                                "Registrazione Completata",
                                "registration",
                                JOptionPane.INFORMATION_MESSAGE);
                    }

                }
                catch (FileNotFoundException err){}
                catch (IOException err){}
                catch (ClassNotFoundException err){}

            }

            private JTextField name;
            private JPasswordField password;
            private JTextField location;
            private boolean action;
            private RegisterWindow actual;


        }
        class back_action implements ActionListener {


            public back_action (LoginWindow in_back, RegisterWindow in_actual){
                back = in_back;
                actual = in_actual;


            }


            @Override
            public void actionPerformed(ActionEvent e)  {
                actual.setVisible(false);
                back.setVisible(true);

            }

            LoginWindow back;
            RegisterWindow actual;


        }
        submit_register.addActionListener(new register_action(this,usert,passt,locationt,true));
        submit_back.addActionListener(new back_action(back,this));



       setSize(220, 180);
       setTitle("Exam APP");
       setVisible(true);
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


    }



      LoginWindow back;

}
