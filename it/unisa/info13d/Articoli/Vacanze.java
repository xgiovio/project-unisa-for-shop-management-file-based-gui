package it.unisa.info13d.Articoli;

import java.io.Serializable;
import java.util.GregorianCalendar;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 16/12/13
 * Time: 18.45
 */

/**
 * Classe che rappresenta una vacanza all'interno del catalogo.
 */
public  class Vacanze extends Utilizzabile implements Serializable{

    /**
     * Costruttore per inizialiazzare una Vacanza.
     *
     * @param idViaggio ide del viaggio
     * @param localitaViaggio localita' del viaggio
     * @param dataPartenzaViaggio data di partenza del viaggio
     * @param scadenzaOfferta scadenza dell'offerta
     * @param prezzoPSingola prezzo per singola persona
     */
    public  Vacanze(int idViaggio, String localitaViaggio, GregorianCalendar dataPartenzaViaggio, GregorianCalendar scadenzaOfferta, double prezzoPSingola){
    	this.idViaggio			 = idViaggio;
    	this.localitaViaggio	 = localitaViaggio;
    	this.dataPartenzaViaggio = dataPartenzaViaggio;
    	this.scadenzaOfferta	 = scadenzaOfferta;
    	this.prezzoPSingola		 = prezzoPSingola;
    }

	@Override
	public boolean eAcquistabile() {
		if(scadenzaOfferta.before(new GregorianCalendar())  || scadenzaOfferta.after(dataPartenzaViaggio) ) //L'offerta della vacanza scade se la data sua scadenza e successiva a quella della data odierna
			return false;
		return true;
	}

    /**
     * @return id del viaggio.
     */
    public int getIdViaggio() {
        return idViaggio;
    }

    /**
     * @return localita' del viaggio.
     */
    public String getLocalitaViaggio() {
        return localitaViaggio;
    }

    /**
     * @return data di partenza.
     */
    public GregorianCalendar getDataPartenzaViaggio() {
        return dataPartenzaViaggio;
    }

    /**
     * @return data di scadenza dell'offerta.
     */
    public GregorianCalendar getScadenzaOfferta() {
        return scadenzaOfferta;
    }

    /**
     * @return il prezzo per singola persona.
     */
    public double getPrezzoPSingola() {
        return prezzoPSingola;
    }

    /**
     * @return il numero di viaggi venduti.
     */
    public int getViaggiVenduti() {
        return viaggiVenduti;
    }

    /**
     * @param localitaViaggio setta la localita' del viaggio.
     */
    public void setLocalitaViaggio(String localitaViaggio) {
        this.localitaViaggio = localitaViaggio;
    }

    /**
     * @param dataPartenzaViaggio setta la data di patenza del viaggio.
     */
    public void setDataPartenzaViaggio(GregorianCalendar dataPartenzaViaggio) {
        this.dataPartenzaViaggio = dataPartenzaViaggio;
    }

    /**
     * @param scadenzaOfferta setta la data di scadenza dell'offerta della vacanza.
     */
    public void setScadenzaOfferta(GregorianCalendar scadenzaOfferta) {
        this.scadenzaOfferta = scadenzaOfferta;
    }

    /**
     * @param prezzoPSingola setta il prezzo per singola persona.
     */
    public void setPrezzoPSingola(double prezzoPSingola) {
        this.prezzoPSingola = prezzoPSingola;
    }
    
    /**
     * Questo metodo incrementa, dopo ogni acquisto, il numero di viaggi venduti.
     */
    public void setViaggiVenduti()
    {
    	viaggiVenduti++;
    }
    
    @Override
	public double get_prezzo_scontato() {
	GregorianCalendar dataOdierna = new GregorianCalendar();
       	
       	long milliseconds1 = dataOdierna.getTimeInMillis();
       	long milliseconds2 = scadenzaOfferta.getTimeInMillis();
       	
       	long diff = milliseconds2 - milliseconds1;
       	long diffGiorni = diff / (24 * 60 * 60 * 1000); //differenza in giorni
       	
    	if( diffGiorni<=7 )
        	return prezzoPSingola - (prezzoPSingola * Global.getSconto_ultima_settimana()   ); //Prezzo scontato
       	else
       		return prezzoPSingola;	//Nessuno sconto applicabile
	}

    private int idViaggio;
	private String localitaViaggio;
	private GregorianCalendar dataPartenzaViaggio;
	private GregorianCalendar scadenzaOfferta;
	private double prezzoPSingola;
	private int viaggiVenduti;
	
}
