package it.unisa.info13d.Articoli;

import java.util.GregorianCalendar;

/**
 * 
 * Classe che rappresenta una prestazione d'opera all'interno del catalogo.
 *
 */
public class PrestazioniDopera extends Utilizzabile {

	/**
	 * Genera una nuova prestazione d'opera da inserire all'interno del catalogo.
	 * 
	 * @param in_id id della prestazione
	 * @param in_localita localit� della prestazione
	 * @param in_descrizione descrizione della prestazione d'opera
	 * @param in_fornitore fornitore della prestazione
	 * @param in_giudizio giudizio del fornitore della prestazione
	 * @param in_prezzo prezzo della prestazione
	 */
    public PrestazioniDopera(int in_id, String in_localita, String in_descrizione, String in_fornitore, int in_giudizio, double in_prezzo){

       idPrOpera = in_id;
       Localita = in_localita;
       Descrizione = in_descrizione;
       Fornitore = in_fornitore;
       giudizioFornitore = in_giudizio;
       prezzoPrestazione = in_prezzo;

    }
	
	@Override
	public boolean eAcquistabile() {
		return true;
	}

	@Override
    public double get_prezzo_scontato() {

            return prezzoPrestazione;	//Nessuno sconto applicabile
    }
	
	/**
	 * 
	 * @return Ritorna l'id dell'opera.
	 */
	public int getIdPrOpera() {
		return idPrOpera;
	}
	
	/**
	 * 
	 * @return Ritorna il giudizio dato al fornitore della prestazione.
	 */
	public int getGiudizioFornitore() {
		return giudizioFornitore;
	}
	
	/**
	 * Questo metodo setta il giudizio dal fornitore della prestazione.
	 * 
	 * @param giudizioFornitore 
	 */
	public void setGiudizioFornitore(int giudizioFornitore) {
		this.giudizioFornitore = giudizioFornitore;
	}
	
	/**
	 * @return Ritorna il numero di prestazioni vendute.
	 */
	public int getPrestazioniVendute() {
		return prestazioniVendute;
	}
	
	/**
	 *Questo metodo incrementa il numero di prestazioni vendute.
	 */
	public void setPrestazioniVendute() {
        prestazioniVendute++;
	}
	
	/**
	 * @return Ritorna il prezzo della prestazione.
	 */
	public double getPrezzoPrestazione() {
		return prezzoPrestazione;
	}
	
	/**
	 * Questo metodo setta il prezzo della prestazione. 
	 * 
	 * @param prezzoPrestazione
	 */
	public void setPrezzoPrestazione(double prezzoPrestazione) {
		this.prezzoPrestazione = prezzoPrestazione;
	}
	
	/**
	 * @return il nome della localita' in cui si pu� fruire della prestazione.
	 */
	public String getLocalita() {
		return Localita;
	}
	
	/** 
	 * Questo metodo setta la localita' della prestazione.
	 * 
	 * @param localita localit� della prestazione
	 */
	public void setLocalita(String localita) {
		Localita = localita;
	}
	
	/**
	 * @return Ritorna la descrizione della prestazione.
	 */
	public String getDescrizione() {
		return Descrizione;
	}
	
	/**
	 * Questo metodo setta la descrizione della prestazione.
	 * 
	 * @param descrizione descrizione della prestazione.
	 */
	public void setDescrizione(String descrizione) {
		Descrizione = descrizione;
	}
	
	/**
	 * @return Ritorna il nome del fornitore della prestazione.
	 */
	public String getFornitore() {
		return Fornitore;
	}
	
	/**
	 * Questo metodo setta il fornitore della prestazione.
	 * 
	 * @param fornitore nome del fornitore
	 */
	public void setFornitore(String fornitore) {
		Fornitore = fornitore;
	}
	
	/**
     * @return la data di scadenza della prestazione.
     */
    public GregorianCalendar getScadenza() {
        return scadenza;
    }

	private int idPrOpera;
	private int giudizioFornitore; // varia da 1 a 5
	private int prestazioniVendute = 0;
	private double prezzoPrestazione;
	private String Localita;
	private String Descrizione;
	private String Fornitore;
    private GregorianCalendar scadenza = new GregorianCalendar(99999,0,1);   
}