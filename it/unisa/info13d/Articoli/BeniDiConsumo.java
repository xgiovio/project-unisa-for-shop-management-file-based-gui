package it.unisa.info13d.Articoli;

import java.io.Serializable;
import java.util.GregorianCalendar;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 16/12/13
 * Time: 18.46
 */

/**
 * 
 * Classe che rappresenta un bene di consumo all'interno del catalogo.
 *
 */
public class BeniDiConsumo extends Utilizzabile implements Serializable {

    /**
     * Costruttore che inizializza i valori del bene di consumo
     *
     * @param idBene id del bene.
     * @param descrizioneBene Descrizione del bene.
     * @param prezzoBene Prezzo del bene.
     * @param beniInStock Quantita' del bene da vendere.
     */
    public BeniDiConsumo(int idBene, String descrizioneBene, double prezzoBene, int beniInStock, String in_fornitore, int in_giudizio){
    	this.idBene=idBene;
    	this.descrizioneBene=descrizioneBene;
    	this.prezzoBene=prezzoBene;
    	this.beniInStock=beniInStock;
        this.giudizioFornitore = in_giudizio;
        this.fornitore = in_fornitore;
    }

	@Override
	public boolean eAcquistabile() {
		if(beniVenduti<beniInStock)
			return true;
		return false;
	}


    /**
     * @return id bene.
     */
    public int getIdBene() {
        return idBene;
    }

    /**
     * Metodo che restituisce la descrizione del bene.
     * 
     * @return descrizione bene.
     */
    public String getDescrizioneBene() {
        return descrizioneBene;
    }

    /**
     * Metodo che restituisce il prezzo del bene.
     * 
     * @return prezzo del bene.
     */
    public double getPrezzoBene() {
        return prezzoBene;
    }

    /**
     * Metodo che restituisce la quantita' dei beni da vendere.
     * 
     * @return la quantita' dei beni da vendere.
     */
    public int getBeniInStock() {
        return beniInStock;
    }

    /**
     * Metodo che restituisce il numero di oggetti venduti.
     * 
     * @return numero di oggetti venduti.
     */
    public int getBeniVenduti() {
        return beniVenduti;
    }

    /**
     * Metodo che setta la descrizione del bene.
     * 
     * @param descrizioneBene descrizione del bene.
     */
    public void setDescrizioneBene(String descrizioneBene) {
        this.descrizioneBene = descrizioneBene;
    }

    /**
     * Metodo che setta il prezzo del bene. 
     * 
     * @param prezzoBene prezzo del bene.
     */
    public void setPrezzoBene(double prezzoBene) {
        this.prezzoBene = prezzoBene;
    }

    /**
     * Metodo che setta la quantita' del bene disponibile per la vendita.
     * 
     * @param beniInStock quantita' del bene disponibile per la vendita.
     */
    public void setBeniInStock(int beniInStock) {
        this.beniInStock = beniInStock;
    }

    /**
     * Metodo che ritorna la scadenza del bene.
     * 
     * @return la scadenza del bene (viene considerata una data a lungo termine).
     */
    public GregorianCalendar getScadenza() {
        return scadenza;
    }

    /**
     * Questo metodo setta i beni venduti, decrementa la quantita' in magazzino e incrementa il numero di oggeti venduti.
     */
    public void setBeniVenduti() {
    	beniInStock--;
    	beniVenduti++;
    }

    /**
     * Metodo che ritorna il giudizione dato al fornitore.
     * 
     * @return Il giudizio dato al fornitore.
     */
    public int getGiudizioFornitore() {
        return giudizioFornitore;
    }

    /**
     * Metodo che ritorna Il nome del fornitore.
     * 
     * @return Il nome del fornitore.
     */
    public String getFornitore() {
        return fornitore;
    }

    @Override
   	public double get_prezzo_scontato() {

       		return prezzoBene;	//Nessuno sconto applicabile
   	}

    private int idBene;
    private int beniInStock; //Numero totale di prodotti da vendere
	private int beniVenduti; //Numero di prodotti venduti
	private int giudizioFornitore; //Giudizio sul fornitore che varia da 1 a 5
	private double prezzoBene;
    private GregorianCalendar scadenza = new GregorianCalendar(99999,0,1);
    private String fornitore;
    private String descrizioneBene;
      
}
