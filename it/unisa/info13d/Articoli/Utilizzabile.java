package it.unisa.info13d.Articoli;

import java.io.Serializable;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 09/01/14
 * Time: 18.11
 */

/**
 * 
 * Classe astratta che definisce i metodi da integrare neglia articoli del catalogo.
 *
 */

public abstract class Utilizzabile implements Serializable {

   /**
    * Metodo per definire se un prodotto e acqustabile
    * 
    * @return Ritorna true se e' acquistabile false se non lo e'
    */
	public abstract boolean eAcquistabile();
    
    /**
     * Metodo per l'applicazione delle politiche di sconto relative ad un articolo.
     * 
     * @return Ritorna il prezzo scontato dell'articolo.
     */
    public abstract double get_prezzo_scontato();
}
