package it.unisa.info13d.Utility;

import it.unisa.info13d.Articoli.Global;
import it.unisa.info13d.Login.Entry;

import java.io.*;
import java.util.ArrayList;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 18/12/13
 * Time: 00:41
 */

/**
 * 
 * Classe che rappresenta il contatore generale dell'id di ogni articolo presente in catalogo
 *
 */
public class IdCounter {

	/**
	 * Questo metodo salva il contatore dell'id sul disco.
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
    public static void save_counter() throws  FileNotFoundException,IOException,ClassNotFoundException{
        File f_data = new File("id_counter");
        Integer data = Integer.valueOf(Global.getGeneral_counter());
        ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(f_data));
        writer.writeObject(data);
        writer.close();
    }

    /**
     * Questo metodo legge il valore del contatore e lo setta.
     * 
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void restore_counter() throws  FileNotFoundException,IOException,ClassNotFoundException{
        File f_data = new File("id_counter");
        if (  (f_data.exists())){
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            Integer data = (Integer) reader.readObject();
            Global.setGeneral_counter(data.intValue());
        }else{

            Global.setGeneral_counter(0);
        }
    }
}
